//
//  TodayViewController.m
//  clock
//
//  Created by Mitesh Bhadrecha on 10/12/14.
//  Copyright (c) 2014 Zensar Technologies. All rights reserved.
//

#import "TodayViewController.h"
#import <NotificationCenter/NotificationCenter.h>

#define RATE_KEY @"kUDRateUsed"

@interface TodayViewController () <NCWidgetProviding>

@end

@implementation TodayViewController

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        
        //add self as observer for the NSUserDefaultsDidChangeNotification
        //catches any changes to the user defaults
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(userDefaultsDidChange:)
                                                     name:NSUserDefaultsDidChangeNotification
                                                   object:nil];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
	[self updateClockSkin];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self updateClockSkin];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)widgetPerformUpdateWithCompletionHandler:(void (^)(NCUpdateResult))completionHandler {
    // Perform any setup necessary in order to update the view.
    
    // If an error is encountered, use NCUpdateResultFailed
    // If there's no update required, use NCUpdateResultNoData
    // If there's an update, use NCUpdateResultNewData
	
}

- (void)userDefaultsDidChange:(NSNotification *)notification {
    
    [self updateClockSkin];
}

//loads the selected skin in the parent app in the widget
- (void)updateClockSkin {
    
    NSUserDefaults *defaults = nil;
    
    #ifndef pro
        defaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.Beetrisa.TimeCandy"];
    #else
        defaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.Beetrisa.TimeCandyPRO"];
    #endif
    
    NSDictionary *skin = [defaults objectForKey:@"selectedSkin"];
    
    if(skin) {
    //set selected skin and add clockview back using addSubview
        [_clockView setClockBackgroundImage:[UIImage imageNamed:[skin objectForKey:@"clockFace"]].CGImage];
        [_clockView setHourHandImage:[UIImage imageNamed:[skin objectForKey:@"hourHand"]].CGImage];
        [_clockView setMinHandImage:[UIImage imageNamed:[skin objectForKey:@"minuteHand"]].CGImage];
        [self.view addSubview:_clockView];
        [_clockView start];
    }
    else {
        #ifndef pro
            [_clockView setClockBackgroundImage:[UIImage imageNamed:@"CH1b.png"].CGImage];
            [_clockView setHourHandImage:[UIImage imageNamed:@"iOS1.png"].CGImage];
            [_clockView setMinHandImage:[UIImage imageNamed:@"iOS2.png"].CGImage];
            [self.view addSubview:_clockView];
            [_clockView start];
        #else
            [_clockView setClockBackgroundImage:[UIImage imageNamed:@"clock001-facet.png"].CGImage];
            [_clockView setHourHandImage:[UIImage imageNamed:@"clock001-hr.png"].CGImage];
            [_clockView setMinHandImage:[UIImage imageNamed:@"clock001-min.png"].CGImage];
            [self.view addSubview:_clockView];
            [_clockView start];
        #endif
    }
}

- (UIEdgeInsets)widgetMarginInsetsForProposedMarginInsets:(UIEdgeInsets)margins {
	
	margins.bottom = 10.0;
	return margins;
}

- (IBAction)openContainerApp:(id)sender {
	
	NSURL *url = [NSURL URLWithString:@"WidgetDemo://"];
	[self.extensionContext openURL:url completionHandler:nil];
}

- (IBAction)clockViewTapped:(id)sender {
	
	NSURL *url = [NSURL URLWithString:@"WidgetDemo://"];
	[self.extensionContext openURL:url completionHandler:nil];
	
}

@end
