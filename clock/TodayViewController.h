//
//  TodayViewController.h
//  clock
//
//  Created by Mitesh Bhadrecha on 10/12/14.
//  Copyright (c) 2014 Zensar Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClockView.h"

@interface TodayViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;
@property (weak, nonatomic) IBOutlet UILabel *lblProgress;

@property (nonatomic, strong) IBOutlet ClockView *clockView;
- (IBAction)openContainerApp:(id)sender;
- (IBAction)clockViewTapped:(id)sender;

@end
