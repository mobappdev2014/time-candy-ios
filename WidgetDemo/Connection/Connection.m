//
//  Connection.m
//  WidgetDemo
//
//  Created by Mitesh Bhadrecha on 11/12/14.
//  Copyright (c) 2014 Zensar Technologies. All rights reserved.
//

#import "Connection.h"

@implementation Connection

//requests weather information using the current location
+ (void)requestCurrentWeatherForLatitude:(float)latitude Longitude:(float)longitude andAPIKey:(NSString *)apiKey withCompletionHandler:(void (^) (BOOL success, NSError *error, NSDictionary *responseDictionary))completionHandler {
    
    NSURL *weatherURL = nil;
    
    weatherURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://api.openweathermap.org/data/2.5/weather?lat=%f&lon=%f&units=metric&APPID=%@", latitude, longitude, apiKey]];
	
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:weatherURL];
    [request setHTTPMethod:@"GET"];
	
	[NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue new] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
		
		if(data && !connectionError) {
            NSError *jsonError = nil;
            NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonError];
			completionHandler(YES, nil, responseDictionary);
		}
		else {
			completionHandler(NO, [NSError errorWithDomain:[connectionError localizedDescription] code:1000 userInfo:nil], nil);
			//show the connectionError
		}
	}];
}

//requests weather information using the current city
+ (void)requestCurrentWeatherForCity:(NSString *)cityName andAPIKey:(NSString *)apiKey withCompletionHandler:(void (^) (BOOL success, NSError *error, NSDictionary *responseDictionary))completionHandler {
    
    NSURL *weatherURL = nil;
    
    weatherURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://api.openweathermap.org/data/2.5/weather?q=%@&units=metric&APPID=%@", cityName, apiKey]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:weatherURL];
    [request setHTTPMethod:@"GET"];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue new] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        if(data && !connectionError) {
            NSError *jsonError = nil;
            NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonError];
            completionHandler(YES, nil, responseDictionary);
        }
        else {
            completionHandler(NO, [NSError errorWithDomain:[connectionError localizedDescription] code:1001 userInfo:nil], nil);
            //show the connectionError
        }
    }];

}

+ (void)fetchWeatherImageWithName:(NSString *)imageName andCompletionHandler:(void (^) (UIImage *image))completionHandler {
    
    NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://openweathermap.org/img/w/%@.png", imageName]];
    
    NSMutableURLRequest *imageRequest = [[NSMutableURLRequest alloc] initWithURL:imageURL];
    [imageRequest setHTTPMethod:@"GET"];
    
    [NSURLConnection sendAsynchronousRequest:imageRequest queue:[NSOperationQueue new] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        if(data && !connectionError) {
            completionHandler([UIImage imageWithData:data]);
        }
    }];
}

@end
