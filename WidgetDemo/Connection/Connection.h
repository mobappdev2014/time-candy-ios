//
//  Connection.h
//  WidgetDemo
//
//  Created by Mitesh Bhadrecha on 11/12/14.
//  Copyright (c) 2014 Zensar Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Connection : NSObject

+ (void)requestCurrentWeatherForLatitude:(float)strLatitude Longitude:(float)strLongitude andAPIKey:(NSString *)apiKey withCompletionHandler:(void (^) (BOOL success, NSError *error, NSDictionary *responseDictionary))completionHandler;

+ (void)requestCurrentWeatherForCity:(NSString *)cityName andAPIKey:(NSString *)apiKey withCompletionHandler:(void (^) (BOOL success, NSError *error, NSDictionary *responseDictionary))completionHandler;

+ (void)fetchWeatherImageWithName:(NSString *)imageName andCompletionHandler:(void (^) (UIImage *image))completionHandler;

@end
