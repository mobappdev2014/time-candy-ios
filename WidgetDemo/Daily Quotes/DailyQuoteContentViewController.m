//
//  DailyQuoteContentViewController.m
//  WidgetDemo
//
//  Created by Salil Shahane on 08/01/15.
//  Copyright (c) 2015 Zensar Technologies. All rights reserved.
//

#import "DailyQuoteContentViewController.h"

@interface DailyQuoteContentViewController ()

@end

@implementation DailyQuoteContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _lblQuote.text = _strQuote;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
