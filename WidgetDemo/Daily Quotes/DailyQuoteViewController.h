//
//  DailyQuoteViewController.h
//  WidgetDemo
//
//  Created by Mitesh Bhadrecha on 24/12/14.
//  Copyright (c) 2014 Zensar Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface DailyQuoteViewController : BaseViewController

- (IBAction)btnHomePressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnPrevious;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;

@end
