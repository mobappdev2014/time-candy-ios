//
//  DailyQuoteContentViewController.h
//  WidgetDemo
//
//  Created by Salil Shahane on 08/01/15.
//  Copyright (c) 2015 Zensar Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DailyQuoteContentViewController : UIViewController

@property NSUInteger pageIndex;
@property (strong, nonatomic) NSString *strQuote;
@property (weak, nonatomic) IBOutlet UILabel *lblQuote;

@end
