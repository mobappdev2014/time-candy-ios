//
//  DailyQuoteViewController.m
//  WidgetDemo
//
//  Created by Mitesh Bhadrecha on 24/12/14.
//  Copyright (c) 2014 Zensar Technologies. All rights reserved.
//

#import "DailyQuoteViewController.h"
#import "DatabaseHelper.h"
#import "QuoteOfTheDay.h"
#import "HomeViewController.h"
#import "WeatherViewController.h"
#import "WatchDesignsViewController.h"
#import "AlarmViewController.h"
#import "DailyQuoteContentViewController.h"
#import "Common.h"
#import "AboutViewController.h"
#import "SettingsViewController.h"

@interface DailyQuoteViewController () <UIActionSheetDelegate> {
    NSArray *quotes;
}

@property BOOL didPressButton;
@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSArray *clockFaces;
@property (strong, nonatomic) NSArray *minuteHands;
@property (strong, nonatomic) NSArray *hourHands;
@property (strong, nonatomic) QuoteOfTheDay *quote;
@property NSUInteger pageIndex;

@end

@implementation DailyQuoteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadAds];
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    
    //don't set dataSource property to disable swipe gesture navigation
    //self.pageViewController.dataSource = self;
    
    //get quotes from database
    [[DatabaseHelper sharedInstance] copyDatabaseIntoDocumentsDirectory];
    quotes = [[DatabaseHelper sharedInstance] fetchQuotes];
    
    DailyQuoteContentViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    // Change the size of page view controller
    self.pageViewController.view.frame = CGRectMake(0, 0, 220, 380);
    [self.pageViewController.view setCenter:self.view.center];
    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
    [_btnPrevious addTarget:self action:@selector(btnPreviousPressed:) forControlEvents:UIControlEventTouchUpInside];
    [_btnNext addTarget:self action:@selector(btnNextPressed:) forControlEvents:UIControlEventTouchUpInside];

    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadBackgroundColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnPreviousPressed:(id)sender {
    _didPressButton = YES;
    if(_pageIndex > 0)
        [self.pageViewController setViewControllers:@[[self viewControllerAtIndex:_pageIndex--]] direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:nil];
}

- (IBAction)btnNextPressed:(id)sender {
    _didPressButton = YES;
    if(_pageIndex < [_hourHands count] - 1)
        [self.pageViewController setViewControllers:@[[self viewControllerAtIndex:_pageIndex++]] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
}

- (DailyQuoteContentViewController *)viewControllerAtIndex:(NSUInteger)index
{
    // Create a new view controller and pass suitable data.
    if(_didPressButton)
        index = _pageIndex;
    
    DailyQuoteContentViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"DailyQuoteContentViewController"];
    QuoteOfTheDay *quote = quotes[index];
    pageContentViewController.strQuote = quote.quote;
    pageContentViewController.pageIndex = index;
    return pageContentViewController;
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    _didPressButton = NO;
    NSUInteger index = ((DailyQuoteContentViewController*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    _didPressButton = NO;
    NSUInteger index = ((DailyQuoteContentViewController*) viewController).pageIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    
    if (index == [self.hourHands count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnHomePressed:(id)sender {
    [self navigateToVC:sender];
}

- (IBAction)btnShareToFacebookPressed:(id)sender {
    _quote = quotes[_pageIndex];
    [Common shareOnFacebookWithMessage:_quote.quote];
}

- (IBAction)btnShareToTwitterPressed:(id)sender {
    _quote = quotes[_pageIndex];
    [Common shareOnTwitterWithMessage:_quote.quote];
}

- (IBAction)btnShareToLinkedInPressed:(id)sender {
    _quote = quotes[_pageIndex];
    [Common authenticateLinkedInUserWithMessage:_quote.quote];
}

- (IBAction)btnMorePressed:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Please select an option" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"About", @"Settings", nil];
    [actionSheet showInView:self.view];
}

#pragma mark - UIActionSheet Delegate Method

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0: {
            AboutViewController *aboutVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutViewController"];
            [self.navigationController pushViewController:aboutVC animated:YES];
            break;
        }
        case 1: {
            SettingsViewController *settingsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
            [self.navigationController pushViewController:settingsVC animated:YES];
            break;
        }
        default:
            break;
    }
}

@end
