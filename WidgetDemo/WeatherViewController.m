//
//  ViewController.m
//  WidgetDemo
//
//  Created by Mitesh Bhadrecha on 10/12/14.
//  Copyright (c) 2014 Zensar Technologies. All rights reserved.
//

#import "WeatherViewController.h"
#import "Common.h"
#import "DatabaseHelper.h"
#import "Connection.h"
#import "HomeViewController.h"
#import <Accounts/Accounts.h>
#import <Social/Social.h>
#import "AlarmViewController.h"
#import "DailyQuoteViewController.h"
#import "WatchDesignsViewController.h"
#import "LIALinkedInAuthorizationViewController.h"
#import "LIALinkedInHttpClient.h"
#import "AboutViewController.h"
#import "SettingsViewController.h"

@interface WeatherViewController () {
    NSMutableDictionary *detailsDictionary;
}

@end

@implementation WeatherViewController

- (void)viewDidLoad {
	[super viewDidLoad];
    
    detailsDictionary = [[NSMutableDictionary alloc] init];
    
    //load ads using Google AdMob SDK
    [self loadAds];
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //load the previously saved weather data that will be shown till the weather updates
    [self loadWeatherViewWithDetails:[[NSUserDefaults standardUserDefaults] objectForKey:@"cachedWeatherData"]];
    
    //get the current latitude and longitude of the user
    float latitude = [[[CurrentLocation sharedInstance] currentLocation] coordinate].latitude;
    
    float longitude = [[[CurrentLocation sharedInstance] currentLocation] coordinate].longitude;
    
    //if the current location is available, fetch the weather data based on the user's location
    //else show an alert prompting the user to input his/her current city
    if([[CurrentLocation sharedInstance] currentLocation]) {
        
        Common *commonObj = [[Common alloc] init];
        [commonObj showLoaderOnView:_imgView withStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [Connection requestCurrentWeatherForLatitude:latitude Longitude:longitude andAPIKey:kAPPID withCompletionHandler:^(BOOL success, NSError *error, NSDictionary *responseDictionary) {
            
            NSArray *weatherArray = [responseDictionary objectForKey:@"weather"];
            NSDictionary *currentWeather = [weatherArray lastObject];
            NSString *iconName = [currentWeather objectForKey:@"icon"];
            
            NSDictionary *mainDictionary = [responseDictionary objectForKey:@"main"];
            [detailsDictionary setObject:[mainDictionary objectForKey:@"temp"] forKey:@"temperature"];
            [detailsDictionary setObject:[mainDictionary objectForKey:@"humidity"] forKey:@"humidity"];
            [detailsDictionary setObject:[responseDictionary objectForKey:@"name"] forKey:@"location_name"];
            [detailsDictionary setObject:[currentWeather objectForKey:@"description"] forKey:@"description"];
            [detailsDictionary setObject:[self getLastUpdatedOn] forKey:@"lastUpdatedOn"];
            [detailsDictionary setObject:iconName forKey:@"iconName"];
            
            dispatch_async(dispatch_get_main_queue(), ^{
               [commonObj hideLoader]; 
            });
        }];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Common getApplicationName] message:@"Please enter your current city" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Search", nil];
        [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
        [alert setTag:1000];
        [alert show];
    }
    [self loadBackgroundColor];
}

#pragma mark - UIAlertView Delegates
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0:
            break;
        case 1: {
            BOOL isCityNameEmpty = [self validateEmptyCityName:alertView];
            
            if(isCityNameEmpty) {
                //call service
                __block NSString *iconName = nil;
                Common *commonObj = [[Common alloc] init];
                [commonObj showLoaderOnView:_imgView withStyle:UIActivityIndicatorViewStyleWhiteLarge];
                [Connection requestCurrentWeatherForCity:[[alertView textFieldAtIndex:0] text] andAPIKey:kAPPID withCompletionHandler:^(BOOL success, NSError *error, NSDictionary *responseDictionary) {
                    if(responseDictionary && !error) {
                        NSDictionary *mainDictionary = [responseDictionary objectForKey:@"main"];
                        NSDictionary *sysDictionary = [responseDictionary objectForKey:@"sys"];
                        NSArray *weatherArray = [responseDictionary objectForKey:@"weather"];
                        NSDictionary *currentWeather = [weatherArray lastObject];
                        iconName = [currentWeather objectForKey:@"icon"];
                        id temp = [mainDictionary objectForKey:@"temp"];
                        int tempInt = [temp intValue];
                        [detailsDictionary setObject:[sysDictionary objectForKey:@"country"] forKey:@"country"];
                        [detailsDictionary setObject:[NSNumber numberWithInteger:tempInt] forKey:@"temperature"];
                        [detailsDictionary setObject:[responseDictionary objectForKey:@"name"] forKey:@"location_name"];
                        [detailsDictionary setObject:[currentWeather objectForKey:@"description"] forKey:@"weatherDescription"];
                        [detailsDictionary setObject:[self getLastUpdatedOn] forKey:@"lastUpdatedOn"];
                        [detailsDictionary setObject:iconName forKey:@"iconName"];
                        dispatch_async(dispatch_get_main_queue(), ^{
                           [self loadWeatherViewWithDetails:detailsDictionary];
                        });
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [commonObj hideLoader];
                        });
                    }
                }];
            }
            else {
                alertView = [[UIAlertView alloc] initWithTitle:[Common getApplicationName] message:@"Please enter a city name to get current weather" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alertView show];
            }
            break;
        }
        default:
            break;
    }
}

//validates if the user has left the city name text field empty in the alert
- (BOOL)validateEmptyCityName:(UIAlertView *)alertView {
    
    if([[[alertView textFieldAtIndex:0] text] length] <= 0)
        return NO;
    else
        return YES;
}

//loads the labels with the needed information
- (void)loadWeatherViewWithDetails:(NSDictionary *)detailDictionary {
    
    [self cacheWeatherDataWithDetails:detailDictionary];
    
    if([[detailDictionary allKeys] count] > 0) {
        [_lblTemperature setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:60]];
        [_lblTemperature setTextColor:[UIColor whiteColor]];
        _lblTemperature.attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@C", [detailDictionary objectForKey:@"temperature"], @"\u00B0"]];
        _lblCity.text = [detailDictionary objectForKey:@"location_name"];
        _lblState.text = [detailDictionary objectForKey:@"country"];
        _lblDescription.text = [detailDictionary objectForKey:@"weatherDescription"];
        _lblLastUpdateOn.text = [NSString stringWithFormat:@"Last updated on : %@", [detailDictionary objectForKey:@"lastUpdatedOn"]];
        _imgView.image = [UIImage imageNamed:[detailDictionary objectForKey:@"iconName"]];
    }
}

//caches weather details which will be shown initially to the user until the weather updates
- (void)cacheWeatherDataWithDetails:(NSDictionary *)detailDictionary {
    
    [[NSUserDefaults standardUserDefaults] setObject:detailDictionary forKey:@"cachedWeatherData"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

//returns a date string that will be saved as the last updated date for the weather
- (NSString *)getLastUpdatedOn {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MMM-dd HH:mm:ss"];

    NSDate *date = [[NSDate alloc] init];
    NSString *strDate = [dateFormatter stringFromDate:date];
    return strDate;
    
}

- (IBAction)homeButtonPressed:(id)sender {
    [self navigateToVC:sender];
}

- (IBAction)btnShareToFacebookPressed:(id)sender {
    [Common shareOnFacebookWithMessage:nil];
}

- (IBAction)btnShareToTwitterPressed:(id)sender {
    [Common shareOnTwitterWithMessage:nil];
}

- (IBAction)btnShareToLinkedInPressed:(id)sender {
    [Common authenticateLinkedInUserWithMessage:nil];
}

- (IBAction)btnMorePressed:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Please select an option" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"About", @"Settings", nil];
    [actionSheet showInView:self.view];
}

#pragma mark - UIActionSheet Delegate Method

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0: {
            AboutViewController *aboutVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutViewController"];
            [self.navigationController pushViewController:aboutVC animated:YES];
            break;
        }
        case 1: {
            SettingsViewController *settingsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
            [self.navigationController pushViewController:settingsVC animated:YES];
            break;
        }
        default:
            break;
    }
}

@end
