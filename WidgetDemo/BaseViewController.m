//
//  BaseViewController.m
//  WidgetDemo
//
//  Created by Salil Shahane on 17/01/15.
//  Copyright (c) 2015 Zensar Technologies. All rights reserved.
//

#import "BaseViewController.h"
#import "WeatherViewController.h"
#import "AlarmViewController.h"
#import "DailyQuoteViewController.h"
#import "WatchDesignsViewController.h"
#import "HomeViewController.h"

@implementation BaseViewController

//shows ads using Google AdMob SDK on a bannerView
- (void)loadAds {
    #ifndef pro
        self.bannerView.adUnitID = @"ca-app-pub-9685298620880953/5087874428";//@"ca-app-pub-3940256099942544/2934735716";
        self.bannerView.rootViewController = self;
        
        GADRequest *request = [GADRequest request];
        // Requests test ads on devices you specify. Your test device ID is printed to the console when
        // an ad request is made.
        //request.testDevices = @[ GAD_SIMULATOR_ID, @"MY_TEST_DEVICE_ID" ];
        [self.bannerView loadRequest:request];
    #endif

}

//method to navigate from the current view to the others.
- (void)navigateToVC:(id)sender {
    BOOL isViewControllerPushed = NO;
    UIViewController *viewController = nil;
    
    NSArray *viewControllers = [self.navigationController viewControllers];
    if([sender tag] == 1) {
        for (UIViewController *vc in viewControllers) {
            if([vc isKindOfClass:[HomeViewController class]]) {
                isViewControllerPushed = YES;
                viewController = vc;
                break;
            }
        }
        if(isViewControllerPushed)
            [self.navigationController popToViewController:viewController animated:YES];
        else {
            [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"] animated:YES];
        }
    }
    else if([sender tag] == 2) {
        for (UIViewController *vc in viewControllers) {
            if([vc isKindOfClass:[WatchDesignsViewController class]]) {
                isViewControllerPushed = YES;
                viewController = vc;
                break;
            }
        }
        if(isViewControllerPushed)
            [self.navigationController popToViewController:viewController animated:YES];
        else {
            [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"WatchDesignsViewController"] animated:YES];
        }
    }
    else if([sender tag] == 3) {//weather
        
        for (UIViewController *vc in viewControllers) {
            if([vc isKindOfClass:[WeatherViewController class]]) {
                isViewControllerPushed = YES;
                viewController = vc;
                break;
            }
        }
        if(isViewControllerPushed)
            [self.navigationController popToViewController:viewController animated:YES];
        else {
            [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"WeatherViewController"] animated:YES];
        }
    }
    else if ([sender tag] == 4) {//quote
        for (UIViewController *vc in viewControllers) {
            if([vc isKindOfClass:[DailyQuoteViewController class]]) {
                isViewControllerPushed = YES;
                viewController = vc;
                break;
            }
        }
        if(isViewControllerPushed)
            [self.navigationController popToViewController:viewController animated:YES];
        else {
            [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"DailyQuoteViewController"] animated:YES];
        }
    }
    else if ([sender tag] == 5) {
        for (UIViewController *vc in viewControllers) {
            if([vc isKindOfClass:[AlarmViewController class]]) {
                isViewControllerPushed = YES;
                viewController = vc;
                break;
            }
        }
        if(isViewControllerPushed)
            [self.navigationController popToViewController:viewController animated:YES];
        else {
            [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"AlarmViewController"] animated:YES];
        }
    }
}

//loads the background color for the view. For daily changing background colors
- (void)loadBackgroundColor {
    if([self isKindOfClass:[HomeViewController class]]) {
        NSDictionary *colorsDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"colorsDict"];
        if([[colorsDict allKeys] count] > 0) {
            NSData *colorData = [colorsDict objectForKey:@"home"];
            if(colorData) {
                UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
                self.view.backgroundColor = color;
            }
        }
    }
    else if ([self isKindOfClass:[WatchDesignsContentViewController class]]) {
        
    }
    else if ([self isKindOfClass:[DailyQuoteViewController class]]) {
        NSDictionary *colorsDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"colorsDict"];
        if([[colorsDict allKeys] count] > 0) {
            NSData *colorData = [colorsDict objectForKey:@"quote"];
            if(colorData) {
                UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
                self.view.backgroundColor = color;
            }
        }
    }
    else if ([self isKindOfClass:[AlarmViewController class]]) {
        
    }
    else if ([self isKindOfClass:[WeatherViewController class]]) {
        NSDictionary *colorsDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"colorsDict"];
        if([[colorsDict allKeys] count] > 0) {
            NSData *colorData = [colorsDict objectForKey:@"weather"];
            if(colorData) {
                UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
                self.view.backgroundColor = color;
            }
        }
    }
}

@end
