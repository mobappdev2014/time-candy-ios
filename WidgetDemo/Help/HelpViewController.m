//
//  HelpViewController.m
//  WidgetDemo
//
//  Created by Salil Shahane on 07/01/15.
//  Copyright (c) 2015 Zensar Technologies. All rights reserved.
//

#import "HelpViewController.h"

@interface HelpViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lblHelp;

@end

@implementation HelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //load ads using Google AdMob SDK
    [self loadAds];
    
    self.navigationItem.title = @"Help";
    
    NSMutableAttributedString *strTitle1 = [[NSMutableAttributedString alloc] initWithString:@"Set the Analog Designer Watch on your home-screen:"];

    [strTitle1 addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:20]
                  range:NSMakeRange(0, [strTitle1 length])];

    NSMutableAttributedString *strTitle2 = [[NSMutableAttributedString alloc] initWithString:@"Alarm, Quote of the Day and Weather Forecast:"];
    [strTitle2 addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:20]
                      range:NSMakeRange(0, [strTitle2 length])];
    
    NSAttributedString *str1 = [[NSAttributedString alloc] initWithString:@"\n\u2022 On the home-screen, go to the WIDGET section.\n\u2022 Tap and hold the Time Candy icon from the list of widgets on your phone and drag it on to the blank home-screen.\n\u2022 You will now see the designer analog watch placed on your home-screen.\n\u2022 To re-size the watch, tap and hold the watch image, drag the dots that appear around the image to re-size\n\u2022 Choose from a range of analog designer watches and customize your home-screen\n\n"];
    
    NSAttributedString *str2 = [[NSAttributedString alloc] initWithString:@"\n\u2022 Set an alarm with customized musical tones.\n\u2022 Library of quotations for a daily dose of inspiration.\n\u2022 Plan your day better with weather forecast" ];
    
    [strTitle1 appendAttributedString:str1];
    [strTitle1 appendAttributedString:strTitle2];
    [strTitle1 appendAttributedString:str2];
    
    [_lblHelp setAttributedText:strTitle1];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnDonePressed:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnBackPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
