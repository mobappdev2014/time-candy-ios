//
//  HelpViewController.h
//  WidgetDemo
//
//  Created by Salil Shahane on 07/01/15.
//  Copyright (c) 2015 Zensar Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface HelpViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet GADBannerView *bannerView;
- (IBAction)btnBackPressed:(id)sender;

@end
