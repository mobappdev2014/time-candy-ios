//
//  QuoteOfTheDay.h
//  WidgetDemo
//
//  Created by Mitesh Bhadrecha on 24/12/14.
//  Copyright (c) 2014 Zensar Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QuoteOfTheDay : NSObject

@property (strong, nonatomic) NSString *quote;
@property BOOL isRead;
@property NSInteger rowID;

@end
