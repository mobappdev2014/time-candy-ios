//
//  NewAlarmViewController.h
//  Awake
//
//  Created by Eliot Fowler on 7/18/13.
//  Copyright (c) 2013 Eliot Fowler. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LabelViewController.h"
#import "RepeatViewController.h"
#import "AlarmSoundsViewController.h"
#import "BaseViewController.h"

@interface NewAlarmViewController : BaseViewController <LabelViewControllerDelegate, RepeatViewControllerDelegate, SelectedAlarmSoundDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;

- (void)scheduleLocalNotification;
- (void)snoozeNotification:(UILocalNotification *)localNotification;
@property (weak, nonatomic) IBOutlet GADBannerView *bannerView;

@end
