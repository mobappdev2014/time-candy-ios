//
//  AboutViewController.m
//  WidgetDemo
//
//  Created by Salil Shahane on 06/01/15.
//  Copyright (c) 2015 Zensar Technologies. All rights reserved.
//

#import "AboutViewController.h"

@interface AboutViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lblAboutApp;

@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //load ads using Google AdMob SDK
    [self loadAds];
    
    self.navigationItem.title = @"About Time Candy";

    _lblAboutApp.text = @"Time Candy brings you a colorful array of unique analog designer watches to dress up your screen.\nYou can set up an alarm with customized quote of the day and musical tones available to you.\nWe keep a close check on the weather of your location to help you plan your day accordingly.\n\n\u00A9 All rights reserved.";
    // Do any additional setup after loading the view.
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnBackPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
