//
//  AboutViewController.h
//  WidgetDemo
//
//  Created by Salil Shahane on 06/01/15.
//  Copyright (c) 2015 Zensar Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface AboutViewController : BaseViewController
- (IBAction)btnBackPressed:(id)sender;
@property (weak, nonatomic) IBOutlet GADBannerView *bannerView;

@end
