//
//  LikeWebViewController.h
//  WidgetDemo
//
//  Created by Salil Shahane on 15/01/15.
//  Copyright (c) 2015 Zensar Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LikeWebViewController : UIViewController <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) NSString *strURL;

- (void)loadRequestFromString:(NSString *)urlString;

@end
