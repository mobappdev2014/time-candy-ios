//
//  SettingsViewController.m
//  WidgetDemo
//
//  Created by Salil Shahane on 16/12/14.
//  Copyright (c) 2014 Zensar Technologies. All rights reserved.
//

#import "HomeViewController.h"
#import "WeatherViewController.h"
#import "AlarmViewController.h"
#import "DailyQuoteViewController.h"
#import "SettingsViewController.h"
#import "AboutViewController.h"
#import "Common.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadAds];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadBackgroundColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"showQuoteSegue"]) {
        DailyQuoteViewController *dailyQuoteVC = [self.storyboard instantiateViewControllerWithIdentifier:@"DailyQuoteViewController"];
        dailyQuoteVC = segue.destinationViewController;
    }
}

#pragma mark - UIAlertView Delegates

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0:
            
            break;
        case 1:
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/keynote/id361285480?mt=8"]];
            break;
        default:
            break;
    }
}

#pragma mark -

- (IBAction)btnShareToFacebookPressed:(id)sender {
    [Common shareOnFacebookWithMessage:nil];
}

- (IBAction)btnShareToTwitterPressed:(id)sender {
    [Common shareOnTwitterWithMessage:nil];
}

- (IBAction)btnShareToLinkedInPressed:(id)sender {
    [Common authenticateLinkedInUserWithMessage:nil];
}

- (IBAction)btnMorePressed:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Please select an option" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"About", @"Settings", nil];
    [actionSheet showInView:self.view];
}

#pragma mark - UIActionSheet Delegate Method

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0: {
            AboutViewController *aboutVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutViewController"];
            [self.navigationController pushViewController:aboutVC animated:YES];
            break;
        }
        case 1: {
            SettingsViewController *settingsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
            [self.navigationController pushViewController:settingsVC animated:YES];
            break;
        }
        default:
            break;
    }
}

- (IBAction)btnWeatherPressed:(id)sender {
    #ifdef pro
        WeatherViewController *weatherVC = [self.storyboard instantiateViewControllerWithIdentifier:@"WeatherViewController"];
        [self.navigationController pushViewController:weatherVC animated:YES];
    #else
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Common getApplicationName] message:@"UPGRADE for ad free version, more watches and musical tones for $0.99 cents" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"UPGRADE", nil];
        [alert show];
    #endif
}

@end
