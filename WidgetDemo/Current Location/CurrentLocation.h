//
//  CurrentLocation.h
//  WidgetDemo
//
//  Created by Mitesh Bhadrecha on 11/12/14.
//  Copyright (c) 2014 Zensar Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface CurrentLocation : NSObject <CLLocationManagerDelegate>

@property (nonatomic, strong) CLLocationManager *locationManager;

+ (CurrentLocation *)sharedInstance;

- (void)initializeLocationServices;

@property (strong, nonatomic) CLLocation *currentLocation;

@end
