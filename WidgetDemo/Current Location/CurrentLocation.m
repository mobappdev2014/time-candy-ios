//
//  CurrentLocation.m
//  WidgetDemo
//
//  Created by Mitesh Bhadrecha on 11/12/14.
//  Copyright (c) 2014 Zensar Technologies. All rights reserved.
//

#import "CurrentLocation.h"

@implementation CurrentLocation

+ (CurrentLocation *)sharedInstance {
    
    static CurrentLocation *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[CurrentLocation alloc] init];
    });
    return sharedInstance;
}

- (void)initializeLocationServices {
	
	_locationManager = [[CLLocationManager alloc] init];
	
	_locationManager.distanceFilter = kCLDistanceFilterNone;
	
	_locationManager.desiredAccuracy = kCLLocationAccuracyBest;
	
	_locationManager.pausesLocationUpdatesAutomatically = YES;
	
    //API change in iOS 8.0
	if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
		[_locationManager requestWhenInUseAuthorization];
	
	[_locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
	
    [_locationManager stopUpdatingLocation];
    
    _currentLocation = [locations lastObject];
    
    NSLog(@"%f, %f", _currentLocation.coordinate.latitude, _currentLocation.coordinate.longitude);
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
	
}

- (void)locationManagerDidPauseLocationUpdates:(CLLocationManager *)manager {
	
}

- (void)locationManagerDidResumeLocationUpdates:(CLLocationManager *)manager {
	
}

@end
