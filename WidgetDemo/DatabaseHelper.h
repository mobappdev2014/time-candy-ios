//
//  DatabaseHelper.h
//  WidgetDemo
//
//  Created by Salil Shahane on 11/12/14.
//  Copyright (c) 2014 Zensar Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface DatabaseHelper : NSObject

@property (strong, nonatomic) NSString *databasePath;
@property (nonatomic) sqlite3 *quotesDB;

+ (DatabaseHelper *)sharedInstance;
- (void)copyDatabaseIntoDocumentsDirectory;
- (NSArray *)fetchQuotes;

@end
