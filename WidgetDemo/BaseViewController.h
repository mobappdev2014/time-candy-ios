//
//  BaseViewController.h
//  WidgetDemo
//
//  Created by Salil Shahane on 17/01/15.
//  Copyright (c) 2015 Zensar Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GADBannerView.h"
#import "GADRequest.h"
#import "LikeWebViewController.h"
#import "Common.h"

@interface BaseViewController : UIViewController <UIActionSheetDelegate>

@property (weak, nonatomic) IBOutlet GADBannerView *bannerView;

- (IBAction)navigateToVC:(id)sender;

- (void)loadAds;

- (void)loadBackgroundColor;

@end
