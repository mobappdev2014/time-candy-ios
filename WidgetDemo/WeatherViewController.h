//
//  ViewController.h
//  WidgetDemo
//
//  Created by Mitesh Bhadrecha on 10/12/14.
//  Copyright (c) 2014 Zensar Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CurrentLocation.h"
#import "BaseViewController.h"
#define kAPPID @"e126084675eabba80e4922d908b25dd8"

@interface WeatherViewController : BaseViewController <UIAlertViewDelegate, UIActionSheetDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblLastUpdateOn;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *lblTemperature;
@property (weak, nonatomic) IBOutlet UILabel *lblCity;
@property (weak, nonatomic) IBOutlet UILabel *lblState;
- (IBAction)homeButtonPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnShareToFacebookPressed;
- (IBAction)btnShareToFacebookPressed:(id)sender;
- (IBAction)btnShareToTwitterPressed:(id)sender;
- (IBAction)btnShareToLinkedInPressed:(id)sender;
@property (weak, nonatomic) IBOutlet GADBannerView *bannerView;


@end

