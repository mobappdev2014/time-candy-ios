//
//  DatabaseHelper.m
//  WidgetDemo
//
//  Created by Salil Shahane on 11/12/14.
//  Copyright (c) 2014 Zensar Technologies. All rights reserved.
//

#define DBName @"quotes.sqlite"

#import "DatabaseHelper.h"
#import "QuoteOfTheDay.h"

@implementation DatabaseHelper

+ (DatabaseHelper *)sharedInstance {
    
    static DatabaseHelper *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[DatabaseHelper alloc] init];
    });
    return sharedInstance;
}

//copies the data base file into the application's document directory
- (void)copyDatabaseIntoDocumentsDirectory {

    //get DB Path
    NSString *docsDir;
    NSArray *dirPaths;
    
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    docsDir = dirPaths[0];
    
    // Build the path to the database file
    _databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent:DBName]];
    
    //copy the db to documents directory
    if (![[NSFileManager defaultManager] fileExistsAtPath:_databasePath]) {
        // The database file does not exist in the documents directory, so copy it from the main bundle now.
        NSString *sourcePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:DBName];
        NSError *error;
        [[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:_databasePath error:&error];
        
        // Check if any error occurred during copying and display it.
        if (error != nil) {
            NSLog(@"%@", [error localizedDescription]);
        }
    }
}

//fetches all the quotes from the data base. Returns an array containing all the quotes fetched
- (NSArray *)fetchQuotes {
	
	NSMutableArray *fetchedQuotes = [[NSMutableArray alloc] init];
	
	const char *dbpath = [_databasePath UTF8String];
    sqlite3_stmt *statement;

    if (sqlite3_open(dbpath, &_quotesDB) == SQLITE_OK)
    {
        NSString *querySQL = @"SELECT * FROM Sheet1";
        
        const char *query_stmt = [querySQL UTF8String];
		
        if (sqlite3_prepare_v2(_quotesDB, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
				QuoteOfTheDay *quote = [[QuoteOfTheDay alloc] init];
                const unsigned char *nullQuoteCheck = sqlite3_column_text(statement, 0);
                if(nullQuoteCheck) {
                    quote.quote = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 0)];
                    quote.isRead = sqlite3_column_int(statement, 1);
                    [fetchedQuotes addObject:quote];
                }
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(_quotesDB);
    }
	return fetchedQuotes;
}

@end
