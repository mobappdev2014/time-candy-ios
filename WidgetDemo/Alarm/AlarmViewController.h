//
//  ViewController.h
//  Awake
//
//  Created by Eliot Fowler on 7/17/13.
//  Copyright (c) 2013 Eliot Fowler. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewAlarmViewController.h"
#import "BaseViewController.h"

@interface AlarmViewController : BaseViewController <UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet GADBannerView *bannerView;
- (IBAction)longPressed:(id)sender;
@property (strong, nonatomic) IBOutlet UILongPressGestureRecognizer *longPressGestureRecognizer;
@property (strong, nonatomic) NSIndexPath *selectedIndexPath;

@end
