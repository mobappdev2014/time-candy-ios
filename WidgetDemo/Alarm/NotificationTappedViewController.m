//
//  NotificationTappedViewController.m
//  WidgetDemo
//
//  Created by Salil Shahane on 08/01/15.
//  Copyright (c) 2015 Zensar Technologies. All rights reserved.
//

#import "NotificationTappedViewController.h"
#import "NewAlarmViewController.h"
#import "AppDelegate.h"
#import "Alarm.h"
#import "DatabaseHelper.h"
#import "QuoteOfTheDay.h"

@interface NotificationTappedViewController () {
    NSArray *quotes;
}

@end

@implementation NotificationTappedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_btnStop setTitle:@"Stop" forState:UIControlStateNormal];
    [_btnSnooze setTitle:@"Snooze" forState:UIControlStateNormal];
    
    [[DatabaseHelper sharedInstance] copyDatabaseIntoDocumentsDirectory];
    quotes = [[DatabaseHelper sharedInstance] fetchQuotes];
    int randomIndex = arc4random() % [quotes count];
    QuoteOfTheDay *quote = [quotes objectAtIndex:randomIndex];
    _lblQuote.text = quote.quote;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnStopPressed:(id)sender {
    
    //get the alarm object id from the userInfo in the localNotification
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSString *strNotificationID = [_firedNotification.userInfo objectForKey:@"alarmObj"];
    NSURL *objectURIRep = [NSURL URLWithString:strNotificationID];
    NSManagedObjectID *objID = [appDelegate.persistentStoreCoordinator managedObjectIDForURIRepresentation:objectURIRep];
    
    //get the Alarm object for the object ID fetched above from the data base
    NSError *error = nil;
    Alarm *alarm = (Alarm *)[appDelegate.managedObjectContext existingObjectWithID:objID error:&error];
    
    //set the next time to fire for the local notification to be the first time the user set the alarm
    //_firedNotification.fireDate = [alarm date];
    [[UIApplication sharedApplication] cancelLocalNotification:_firedNotification];
    
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = [alarm date];
    localNotification.alertBody = [alarm title];
    localNotification.timeZone = [NSTimeZone localTimeZone];
    localNotification.alertAction = @"I'm up, I'm up!";
    localNotification.soundName = [NSString stringWithFormat:@"%@.m4a", [alarm sound]];
    localNotification.repeatInterval = NSWeekCalendarUnit;
    localNotification.userInfo = @{@"alarmObj":[[alarm.objectID URIRepresentation] absoluteString]};
    
    //schedule the notification again
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    
    //dismiss the quote VC
    [appDelegate.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnSnoozePressed:(id)sender {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSDate *selectedDate = [[NSDate alloc] init];
    NSString *strSelectedDate = [dateFormatter stringFromDate:selectedDate];
    NSDate *currentDate = [dateFormatter dateFromString:strSelectedDate];
    
    //get the alarm object id from the userInfo in the localNotification
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSError *error = nil;
    NSString *strNotificationID = [_firedNotification.userInfo objectForKey:@"alarmObj"];
    NSURL *objectURIRep = [NSURL URLWithString:strNotificationID];
    NSManagedObjectID *objID = [appDelegate.persistentStoreCoordinator managedObjectIDForURIRepresentation:objectURIRep];
    
    //get the Alarm object for the object ID fetched above from the data base
    Alarm *alarm = (Alarm *)[appDelegate.managedObjectContext existingObjectWithID:objID error:&error];
    
    //if snooze time is set, set the next fire date to the current time + snooze time.
    //else set a default snooze time of 5 minutes.
    NSDate *nextDate = nil;
    if([alarm.snoozeLength integerValue] > 0)
        nextDate = [NSDate dateWithTimeInterval:[alarm.snoozeLength integerValue] * 60 sinceDate:currentDate];
    else {
        nextDate = [NSDate dateWithTimeInterval:[[NSNumber numberWithInteger:5] integerValue] * 60 sinceDate:currentDate];
    }
    
    _firedNotification.fireDate = nextDate;
    
    //schedule the notification again
    [[UIApplication sharedApplication] scheduleLocalNotification:_firedNotification];
    
    //dismiss the quote VC
    [appDelegate.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}


@end
