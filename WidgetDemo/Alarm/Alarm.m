//
//  Alarm.m
//  WidgetDemo
//
//  Created by Mitesh Bhadrecha on 19/12/14.
//  Copyright (c) 2014 Zensar Technologies. All rights reserved.
//

#import "Alarm.h"


@implementation Alarm

@dynamic date;
@dynamic isSet;
@dynamic repeatString;
@dynamic snoozeLength;
@dynamic sound;
@dynamic title;

@end
