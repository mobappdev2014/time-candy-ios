//
//  LabelViewController.h
//  Awake
//
//  Created by Eliot Fowler on 7/24/13.
//  Copyright (c) 2013 Eliot Fowler. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@class LabelViewController;

@protocol LabelViewControllerDelegate;

@interface LabelViewController : BaseViewController <UITextFieldDelegate>
@property (nonatomic, assign) id <LabelViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet GADBannerView *bannerView;
@end

@protocol LabelViewControllerDelegate <NSObject>
@required
- (void)didFinishEditingLabel:(NSString*)label;
@end
