//
//  ViewController.m
//  Awake
//
//  Created by Eliot Fowler on 7/17/13.
//  Copyright (c) 2013 Eliot Fowler. All rights reserved.
//

#import "AlarmViewController.h"
#import "NewAlarmViewController.h"
#import "AlarmCell.h"
#import "UIImage+ImageEffects.h"
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "AlarmData.h"
#import "HomeViewController.h"
#import "WatchDesignsViewController.h"
#import "WeatherViewController.h"
#import "DailyQuoteViewController.h"
#import "AboutViewController.h"
#import "Common.h"
#import "SettingsViewController.h"
#import "Alarm.h"

@interface AlarmViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, NSFetchedResultsControllerDelegate, UIActionSheetDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *alarmListCollectionView;
@property (weak, nonatomic) IBOutlet UILabel *alarmListEmptyLabel;
@property (nonatomic, strong) NSMutableArray *alarmListArray;
@property (nonatomic, strong) NewAlarmViewController* alarmController;
@property (weak, nonatomic) IBOutlet UISwitch *switchOnCell;
@property (strong, nonatomic) UIButton *addAlarm;
@property (nonatomic, strong) UINavigationController *modalNavController;

@end

@implementation AlarmViewController {
    NSFetchedResultsController* fetchedResultsController;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        _alarmListArray = [NSMutableArray array];
    }
    return self;
}
- (IBAction)didClickAddButton:(id)sender {
        
	if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)])
		UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, NO, [UIScreen mainScreen].scale);
	else
		UIGraphicsBeginImageContext(self.view.bounds.size);
	
	[self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
	UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	UIImage *effectImage = nil;
	effectImage = [image applyLightEffect];
	
	_alarmController.backgroundImage.image = effectImage;
	
	NewAlarmViewController *newAlarmVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NewAlarmViewController"];
	
	[self.navigationController pushViewController:newAlarmVC animated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadAds];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Alarm" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    fetchedResultsController = [[NSFetchedResultsController alloc]
                                initWithFetchRequest:fetchRequest
                                managedObjectContext:context
                                sectionNameKeyPath:nil
                                cacheName:nil];
    
    fetchedResultsController.delegate = self;
    
    NSError *error;
    BOOL success = [fetchedResultsController performFetch:&error];
    if(!success) {
        NSLog(@"There was ab error fetching data: %@", error);
    }
    [self reloadCollectionViewData];
    _modalNavController = [self.storyboard instantiateViewControllerWithIdentifier:@"NewAlarmViewController"];
    
	[self reloadCollectionViewData];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self loadBackgroundColor];
//    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//    
//    //set add alarm button properties
//    _addAlarm = [[UIButton alloc] initWithFrame:CGRectMake(280, 64, 30, 30)];
//    [_addAlarm.layer setCornerRadius:_addAlarm.frame.size.width/2];
//    [_addAlarm.layer setShadowColor:[UIColor blackColor].CGColor];
//    [_addAlarm.layer setShadowOffset:CGSizeMake(0.0f, 1.0f)];
//    [_addAlarm.layer setShadowRadius:2.0f];
//    [_addAlarm.layer setShadowOpacity:0.5f];
//    [_addAlarm.layer setMasksToBounds:NO];
//    [_addAlarm setImage:[UIImage imageNamed:@"ic_floating_action_blue"] forState:UIControlStateNormal];
//    //[_addAlarm setBackgroundColor:[UIColor colorWithRed:(23.0f/255.0f) green:(112.0f/255.0f) blue:(162.0f/255.0f) alpha:1.0f]];
//    
//    //add pan gesture for dragging
//    UIPanGestureRecognizer *pangr = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(pan:)];
//    [_addAlarm addGestureRecognizer:pangr];
//    
//    //add target for button click
//    [_addAlarm addTarget:self action:@selector(didClickAddButton:) forControlEvents:UIControlEventTouchUpInside];
//    //add the button on the window so it is always visible above other components
//    [appDelegate.window addSubview:_addAlarm];
    
    //reload collection view
    [self reloadCollectionViewData];
}

//pan gesture handler for button dragging
//- (void)pan:(UIPanGestureRecognizer *)recognizer
//{
//    if (recognizer.state == UIGestureRecognizerStateChanged ||
//        recognizer.state == UIGestureRecognizerStateEnded) {
//        
//        UIView *draggedButton = recognizer.view;
//        CGPoint translation = [recognizer translationInView:self.view];
//        
//        CGRect newButtonFrame = draggedButton.frame;
//        newButtonFrame.origin.x += translation.x;
//        newButtonFrame.origin.y += translation.y;
//        if((newButtonFrame.origin.x > 0 && newButtonFrame.origin.x < self.view.frame.size.width - 30) && (newButtonFrame.origin.y > 64 && newButtonFrame.origin.y < self.view.frame.size.height - 30))
//        draggedButton.frame = newButtonFrame;
//        NSLog(@"New Frame : %f", newButtonFrame.origin.x);
//        [recognizer setTranslation:CGPointZero inView:self.view];
//    }
//}

- (void)viewWillDisappear:(BOOL)animated {
    
    [_addAlarm removeFromSuperview];
    [super viewWillDisappear:animated];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
                                                                atIndexPath:(NSIndexPath *)indexPath
                                                              forChangeType:(NSFetchedResultsChangeType)type
                                                               newIndexPath:(NSIndexPath *)newIndexPath {
    UICollectionView* collectionView = self.alarmListCollectionView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            NSLog(@"Inserting fetched result");
            [collectionView insertItemsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]];
            break;
            
        case NSFetchedResultsChangeDelete:
            NSLog(@"Deleting fetched result");
            [collectionView deleteItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
            break;
            
        case NSFetchedResultsChangeUpdate:
            NSLog(@"Updating fetched result");
            break;
            
        case NSFetchedResultsChangeMove:
            NSLog(@"Moving fetched result");
            [collectionView deleteItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
            [collectionView insertItemsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]];
            break;
    }
    
}

- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return [[fetchedResultsController sections] count];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if ([[fetchedResultsController sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[fetchedResultsController sections] objectAtIndex:section];
        return [sectionInfo numberOfObjects];
    } else
        return 0;
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    AlarmCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    NSManagedObject *managedObject = [fetchedResultsController objectAtIndexPath:indexPath];

    BOOL isSet = [(NSNumber*)[managedObject valueForKey:@"isSet"] boolValue];
    NSString* title = [managedObject valueForKey:@"title"];
    NSDate* date = [managedObject valueForKey:@"date"];
    NSString* repeatString = [managedObject valueForKey:@"repeatString"];
    
//    CGFloat alpha = isSet ? 1.0f : 0.3f;
    //cell.backgroundColor = [UIColor colorWithRed:105/255.0 green:195/255.0 blue:255/255.0 alpha:alpha];
    [cell.switchOnCell setOn:isSet];
    cell.titleLabel.text = title;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setDateFormat:@"h:mm a"];
    cell.alarmTimeLabel.text = [dateFormatter stringFromDate:date];
    cell.repeatLabel.text = repeatString;
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.view.frame.size.width, 80);
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"showRepeatView"]){
        NewAlarmViewController *controller = (NewAlarmViewController *)segue.destinationViewController;
        
        if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)])
            UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, NO, [UIScreen mainScreen].scale);
        else
            UIGraphicsBeginImageContext(self.view.bounds.size);
        
        [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        controller.backgroundImage.image = image;
    }
}

- (void)scheduleLocalNotificationForSelectedTimeWithObject:(Alarm *)alarmObj {
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    NSDate *currentDate = [NSDate date];
    NSDate *savedDate = [alarmObj date];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss z"];
    NSLog(@"%@", [dateFormatter stringFromDate:currentDate]);
    [dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *savedTime = [dateFormatter stringFromDate:savedDate];
    
    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    [gregorian setLocale:[NSLocale currentLocale]];
    
    NSDateComponents *savedComponents = [gregorian components:NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:[alarmObj date]];
    
    NSDateComponents *currentDateComponents = [gregorian components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:currentDate];

    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setYear:currentDateComponents.year];
    [dateComponents setMonth:currentDateComponents.month];
    [dateComponents setDay:currentDateComponents.day];
    [dateComponents setHour:savedComponents.hour];
    [dateComponents setMinute:savedComponents.minute];
    [dateComponents setSecond:savedComponents.second];
    
    NSDate *fireDate = [gregorian dateFromComponents:dateComponents];
    
    localNotification.fireDate = fireDate;
    localNotification.timeZone = [NSTimeZone localTimeZone];
    localNotification.alertBody = self.title;
    localNotification.alertAction = @"I'm up, I'm up!";
    localNotification.soundName = [NSString stringWithFormat:@"%@.m4a", [alarmObj sound]];
    localNotification.repeatInterval = NSWeekCalendarUnit;
    localNotification.userInfo = @{@"alarmObj":[[alarmObj.objectID URIRepresentation] absoluteString]};
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

- (NSDate *)nextDate:(NSInteger)dayofWeek withObject:(Alarm *)alarmObj {
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    [gregorian setLocale:[NSLocale currentLocale]];
    
    NSDateComponents *savedComponents = [gregorian components:NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:[alarmObj date]];
    
    NSDateComponents *currentDateComponents = [gregorian components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:[NSDate date]];
    
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setYear:currentDateComponents.year];
    [dateComponents setMonth:currentDateComponents.month];
    [dateComponents setDay:currentDateComponents.day];
    [dateComponents setHour:savedComponents.hour];
    [dateComponents setMinute:savedComponents.minute];
    [dateComponents setSecond:savedComponents.second];
    
    NSDate *fireDate = [gregorian dateFromComponents:dateComponents];
    
    
    NSDateComponents *nowComponents = [gregorian components:NSYearCalendarUnit | NSWeekCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:fireDate];
    
    [nowComponents setWeekday:dayofWeek]; //Monday
    [nowComponents setWeekOfMonth:[nowComponents weekOfMonth] + 1];
    NSDate *beginningOfWeek = [gregorian dateFromComponents:nowComponents];
    return beginningOfWeek;
}

- (IBAction)rescheduleStoppedAlarm:(id)sender {
    
    UISwitch* alarmSwitch = (UISwitch *)sender;
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.alarmListCollectionView];
    NSIndexPath* indexPath = [self.alarmListCollectionView indexPathForItemAtPoint:buttonPosition];
    Alarm *alarm = [fetchedResultsController objectAtIndexPath:indexPath];
    NSDate *today = [NSDate date];
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    if(![alarmSwitch isOn]) {
        
        NSManagedObjectID *objID = [alarm objectID];
        
        NSArray *scheduledNotifications = [[UIApplication sharedApplication] scheduledLocalNotifications];
        
        [scheduledNotifications enumerateObjectsUsingBlock:^(UILocalNotification *localNotification, NSUInteger idx, BOOL *stop) {
            if([[localNotification.userInfo objectForKey:@"alarmObj"] isEqualToString:[[objID URIRepresentation] absoluteString]])
                [[UIApplication sharedApplication] cancelLocalNotification:localNotification];
        }];
    }
    else {
        NSString *str = [alarm.repeatString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        NSArray *repeatDays = [str componentsSeparatedByString:@" "];
        
        [self scheduleLocalNotificationForSelectedTimeWithObject:alarm];

        [repeatDays enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if([repeatDays containsObject:obj]) {
                
                if([obj isEqualToString:@"Su"])
                    localNotification.fireDate = [self nextDate:1 withObject:alarm];
                
                if([obj isEqualToString:@"M"])
                    localNotification.fireDate = [self nextDate:2 withObject:alarm];
                
                if([obj isEqualToString:@"T"])
                    localNotification.fireDate = [self nextDate:3 withObject:alarm];
                
                if([obj isEqualToString:@"W"])
                    localNotification.fireDate = [self nextDate:4 withObject:alarm];
                
                if([obj isEqualToString:@"R"])
                    localNotification.fireDate = [self nextDate:5 withObject:alarm];
                
                if([obj isEqualToString:@"F"])
                    localNotification.fireDate = [self nextDate:6 withObject:alarm];
                
                if([obj isEqualToString:@"Sa"])
                    localNotification.fireDate = [self nextDate:7 withObject:alarm];
                
                //UILocalNotification *localNotification = [[UILocalNotification alloc] init];
                //localNotification.fireDate = //alarm.date;
                localNotification.alertBody = [alarm title];
                localNotification.timeZone = [NSTimeZone localTimeZone];
                localNotification.alertAction = [alarm title];
                
                localNotification.soundName = [NSString stringWithFormat:@"%@.m4a", alarm.sound];
                
                localNotification.repeatInterval = NSWeekCalendarUnit;
                
                localNotification.userInfo = @{@"alarmObj":[[alarm.objectID URIRepresentation] absoluteString]};
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            else {
                if([[alarm date] earlierDate:today] == today) {
                    localNotification.fireDate = [alarm date];
                    localNotification.alertBody = @"Title";
                    localNotification.timeZone = [NSTimeZone localTimeZone];
                    
                    localNotification.soundName = [NSString stringWithFormat:@"%@.m4a", [alarm sound]];
                    
                    localNotification.alertAction = @"I'm up, I'm up!";
                    
                    localNotification.userInfo = @{@"alarmObj":[[alarm.objectID URIRepresentation] absoluteString]};
                    
                    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
                }
            }
        }];
//        for(id key in repeatDays) {
//            if([repeatDays containsObject:key]) {
//                UILocalNotification *localNotification = [[UILocalNotification alloc] init];
//                localNotification.fireDate = alarm.date;
//                localNotification.alertBody = [alarm title];
//                localNotification.timeZone = [NSTimeZone localTimeZone];
//                localNotification.alertAction = [alarm title];
//                
//                localNotification.soundName = [NSString stringWithFormat:@"%@.m4a", alarm.sound];
//                
//                localNotification.repeatInterval = NSCalendarUnitWeekday;
//                
//                localNotification.userInfo = @{@"alarmObj":[[alarm.objectID URIRepresentation] absoluteString]};
//                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
//            }
//            else {
//                if([[alarm date] earlierDate:today] == today) {
//                    localNotification.fireDate = [alarm date];
//                    localNotification.alertBody = @"Title";
//                    localNotification.timeZone = [NSTimeZone localTimeZone];
//                    
//                    localNotification.soundName = [NSString stringWithFormat:@"%@.m4a", [alarm sound]];
//                    
//                    localNotification.alertAction = @"I'm up, I'm up!";
//                    
//                    localNotification.userInfo = @{@"alarmObj":[[alarm.objectID URIRepresentation] absoluteString]};
//                    
//                    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
//                }
//            }
        //}
    }
}

- (IBAction)switchWasSwitched:(id)sender {
    UISwitch* alarmSwitch = (UISwitch *)sender;
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.alarmListCollectionView];
    NSIndexPath* indexPath = [self.alarmListCollectionView indexPathForItemAtPoint:buttonPosition];
    NSManagedObject* managedObject = [fetchedResultsController objectAtIndexPath:indexPath];
    NSNumber* isOnNumber = [NSNumber numberWithBool:[alarmSwitch isOn]];
    
    [self rescheduleStoppedAlarm:sender];
    
    [managedObject setValue:isOnNumber forKey:@"isSet"];
    
    NSManagedObjectContext *context = [fetchedResultsController managedObjectContext];
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }
    
    //AlarmCell* cell = (AlarmCell*)[self.alarmListCollectionView cellForItemAtIndexPath:indexPath];
//    cell.backgroundColor = [cell.backgroundColor colorWithAlphaComponent:[alarmSwitch isOn] ? 1.0f : 0.3f];
}

#pragma mark - Helpers

- (void)reloadCollectionViewData
{
    [_alarmListCollectionView reloadData];
    
    BOOL hasItems = [[fetchedResultsController fetchedObjects] count] > 0;
    
    [_alarmListCollectionView setHidden:!hasItems];
    [_alarmListEmptyLabel setHidden:hasItems];
}


//- (IBAction)swipedRight:(id)sender {
//	
//	UISwipeGestureRecognizer *swipe = sender;
//
//	//get the cell to delete
//	AlarmCell *cell = (AlarmCell *)[swipe view];
//	
//	//get the indexpath for deleting object
//	NSIndexPath *indexPath = [_alarmListCollectionView indexPathForCell:cell];
//	
//	//delete object from DB and save context
//	AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//	[[appDelegate managedObjectContext] deleteObject:[fetchedResultsController objectAtIndexPath:indexPath]];
//	NSError *error = nil;
//	[[appDelegate managedObjectContext] save:&error];
//	
////	//delete item from indexpath from collection view
////	[_alarmListCollectionView deleteItemsAtIndexPaths:@[indexPath]];
//	
//	//reload collectionview
//	[self reloadCollectionViewData];
//}

- (IBAction)homeButtonPressed:(id)sender {
    [self navigateToVC:sender];
}

- (IBAction)btnMorePressed:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Please select an option" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"About", @"Settings", nil];
    [actionSheet showInView:self.view];
}

#pragma mark - UIActionSheet Delegate Method

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0: {
            AboutViewController *aboutVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutViewController"];
            [self.navigationController pushViewController:aboutVC animated:YES];
            break;
        }
        case 1: {
            SettingsViewController *settingsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
            [self.navigationController pushViewController:settingsVC animated:YES];
            break;
        }
        default:
            break;
    }
}

- (IBAction)btnShareToFacebookPressed:(id)sender {
    [Common shareOnFacebookWithMessage:nil];
}

- (IBAction)btnShareToTwitterPressed:(id)sender {
    [Common shareOnTwitterWithMessage:nil];
}

- (IBAction)btnShareToLinkedInPressed:(id)sender {
    [Common authenticateLinkedInUserWithMessage:nil];
}

- (IBAction)longPressed:(id)sender {
    _longPressGestureRecognizer = (UILongPressGestureRecognizer *)sender;
    if([_longPressGestureRecognizer state] == UIGestureRecognizerStateBegan) {
        CGPoint point = [_longPressGestureRecognizer locationInView:self.alarmListCollectionView];
        _selectedIndexPath = [self.alarmListCollectionView indexPathForItemAtPoint:point];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Common getApplicationName] message:@"Do you want to delete the alarm ? " delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        [alert show];
    }
}

#pragma mark - UIAlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0:
            
            break;
        case 1: {
            [self.alarmListCollectionView performBatchUpdates:^{

                //delete object from DB and save context
                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                
                //get object for corresponding indexpath from data base
                Alarm *alarm = [fetchedResultsController objectAtIndexPath:_selectedIndexPath];
                
                //get the object ID for the fetched object
                NSManagedObjectID *objID = [alarm objectID];
                
                //get list of all the scheduled notifications
                NSArray *scheduledNotifications = [[UIApplication sharedApplication] scheduledLocalNotifications];
                
                //iterate through the list and delete localnotifications having id matching objID
                [scheduledNotifications enumerateObjectsUsingBlock:^(UILocalNotification *localNotification, NSUInteger idx, BOOL *stop) {
                    if([[localNotification.userInfo objectForKey:@"alarmObj"] isEqualToString:[[objID URIRepresentation] absoluteString]])
                        [[UIApplication sharedApplication] cancelLocalNotification:localNotification];
                }];
                
                //delete object from data base
                [[appDelegate managedObjectContext] deleteObject:[fetchedResultsController objectAtIndexPath:_selectedIndexPath]];
                
                NSError *error = nil;
                
                //save database
                [[appDelegate managedObjectContext] save:&error];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    //reload collectionview
                    [self reloadCollectionViewData];
                });
            } completion:^(BOOL finished) {
                
            }];
            break;
        }
        default:
            break;
    }
}

@end
