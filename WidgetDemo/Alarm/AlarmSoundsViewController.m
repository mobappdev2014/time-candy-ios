//
//  AlarmSoundsViewController.m
//  WidgetDemo
//
//  Created by Salil Shahane on 30/12/14.
//  Copyright (c) 2014 Zensar Technologies. All rights reserved.
//

#import "AlarmSoundsViewController.h"
#import "Common.h"

@interface AlarmSoundsViewController () {
    NSMutableArray *freeTonesArray;
}

@property (strong, nonatomic) NSMutableArray *soundNames;

@end

@implementation AlarmSoundsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadAds];
    
    _soundNames = [[NSMutableArray alloc] init];
    
    alarmSounds = [[NSBundle mainBundle] pathsForResourcesOfType:@"m4a" inDirectory:nil];
    
    [_soundNames addObject:@"Default"];
    
    for (NSString *str in alarmSounds) {
        [_soundNames addObject:[[str lastPathComponent] stringByDeletingPathExtension]];
    }

    #ifndef pro
       freeTonesArray = [[NSMutableArray alloc] initWithArray:[_soundNames subarrayWithRange:NSMakeRange(0, 4)]];
    #endif
    
    [_soundNames sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        //Default compare, to protect against cast
        if (![obj1 isKindOfClass:[NSString class]] || ![obj2 isKindOfClass:[NSString class]]) {
            return ([obj1 compare:obj2]);
        }
        else {
            NSString *aString = (NSString*) obj1;
            NSString *bString = (NSString*) obj2;
            int aInt = [[aString substringFromIndex:3] intValue];
            int bInt = [[bString substringFromIndex:3] intValue];
            return aInt < bInt;
        }
    }];
    
    UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(alarmSoundSelected:)];
    [self.navigationItem setRightBarButtonItem:doneItem];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnCancelPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)alarmSoundSelected:(id)sender {
    
    if(_selectedCell) {
        [_delegate selectedAlarmSound:_selectedCell.textLabel.text];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Common getApplicationName] message:@"Please select a tone to be played" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    #ifndef pro
        return [freeTonesArray count];
    #else
        return [_soundNames count];
    #endif
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"soundsCell" forIndexPath:indexPath];
    NSString *theFileName = nil;
    // Configure the cell...
#ifndef pro
    theFileName = [freeTonesArray objectAtIndex:indexPath.row];
#else
    theFileName = [_soundNames objectAtIndex:indexPath.row];
#endif
//    [[[alarmSounds objectAtIndex:indexPath.row] lastPathComponent] stringByDeletingPathExtension];
    [cell setTintColor:[UIColor blackColor]];
    cell.textLabel.text = theFileName;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
    _selectedCell = cell;
    
    NSURL *soundFileURL = nil;
    if(indexPath.row == 0)
        soundFileURL = [NSURL fileURLWithPath:[_soundNames objectAtIndex:indexPath.row]];
    else
        soundFileURL = [NSURL fileURLWithPath:[alarmSounds objectAtIndex:indexPath.row - 1]];
    
    _player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    
    [_player setVolume:1.0];
    
    _player.numberOfLoops = -1; //Infinite
    
    [_player play];
    
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
