//
//  NotificationTappedViewController.h
//  WidgetDemo
//
//  Created by Salil Shahane on 08/01/15.
//  Copyright (c) 2015 Zensar Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationTappedViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *btnStop;
@property (weak, nonatomic) IBOutlet UIButton *btnSnooze;
@property (strong, nonatomic) UILocalNotification *firedNotification;
- (IBAction)btnStopPressed:(id)sender;
- (IBAction)btnSnoozePressed:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblQuote;

@end
