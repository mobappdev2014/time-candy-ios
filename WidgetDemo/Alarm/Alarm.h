//
//  Alarm.h
//  WidgetDemo
//
//  Created by Mitesh Bhadrecha on 19/12/14.
//  Copyright (c) 2014 Zensar Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Alarm : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSNumber * isSet;
@property (nonatomic, retain) NSString * repeatString;
@property (nonatomic, retain) NSNumber * snoozeLength;
@property (nonatomic, retain) NSString * sound;
@property (nonatomic, retain) NSString * title;

@end
