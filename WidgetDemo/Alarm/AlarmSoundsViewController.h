//
//  AlarmSoundsViewController.h
//  WidgetDemo
//
//  Created by Salil Shahane on 30/12/14.
//  Copyright (c) 2014 Zensar Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@import AudioToolbox;
@import AVFoundation;

@protocol SelectedAlarmSoundDelegate <NSObject>

- (void)selectedAlarmSound:(NSString *)filePath;

@end

@interface AlarmSoundsViewController : BaseViewController {
    NSArray *alarmSounds;
}

@property (strong, nonatomic) id <SelectedAlarmSoundDelegate> delegate;
@property (strong, nonatomic) AVAudioPlayer *player;
@property (strong, nonatomic) UITableViewCell *selectedCell;
@property (weak, nonatomic) IBOutlet GADBannerView *bannerView;

@end
