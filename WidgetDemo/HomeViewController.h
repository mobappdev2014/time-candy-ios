//
//  SettingsViewController.h
//  WidgetDemo
////  Created by Salil Shahane on 16/12/14.
//  Copyright (c) 2014 Zensar Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface HomeViewController : BaseViewController <UIActionSheetDelegate, UIAlertViewDelegate>

- (IBAction)btnMorePressed:(id)sender;

@property (weak, nonatomic) IBOutlet GADBannerView *bannerView;
- (IBAction)btnWeatherPressed:(id)sender;

@end
