//
//  AppDelegate.m
//  WidgetDemo
//
//  Created by Mitesh Bhadrecha on 10/12/14.
//  Copyright (c) 2014 Zensar Technologies. All rights reserved.
//

#import "AppDelegate.h"
#import "CurrentLocation.h"
#import "DatabaseHelper.h"
#import "NewAlarmViewController.h"
#import "NotificationTappedViewController.h"
#import "Common.h"
#import "HomeViewController.h"
#import "WatchDesignsContentViewController.h"
#import "DailyQuoteViewController.h"
#import "AlarmViewController.h"
#import "WeatherViewController.h"

@interface AppDelegate () {
    UILocalNotification *firedNotification;
}

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
	// Override point for customization after application launch.
	
    NSLog(@"%@", launchOptions);
    
    if([[launchOptions allKeys] containsObject:UIApplicationLaunchOptionsLocalNotificationKey]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        NotificationTappedViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"NotificationTappedViewController"];
        UILocalNotification *notification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
        
        UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:@"RootController"];
        self.window.rootViewController = navController;
        
        viewController.firedNotification = notification;
        NSLog(@"%@", self.window.rootViewController);
        [self.window makeKeyAndVisible];
        [self.window.rootViewController presentViewController:viewController animated:YES completion:nil];
    }
    
	if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)]) {
		[[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeSound
																											  categories:nil]];
	}
	
    //fix for previously scheduled notifications firing after app delete
    if(![[NSUserDefaults standardUserDefaults] objectForKey:@"cancelledPreviousNotifications"]) {
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
        [[NSUserDefaults standardUserDefaults] setObject:@YES forKey:@"cancelledPreviousNotifications"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    [[CurrentLocation sharedInstance] initializeLocationServices];
    
    if(![[NSUserDefaults standardUserDefaults] objectForKey:@"previousDate"]) {
        [[NSUserDefaults standardUserDefaults] setObject:[[NSDate alloc] init] forKey:@"previousDate"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
	
	return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
	// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
	// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
	// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
	// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
	// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [self shuffleBackgroundColors];
}

//shuffles background colors for all view controllers after 24 hours have passed since the previously saved date
- (void)shuffleBackgroundColors {
    if([Common hasDayElapsed]) {
        
        //get the root view controller
        UINavigationController *navController = (UINavigationController *)self.window.rootViewController;

        //get the top view controller from the root view controller
        UIViewController *vc = [navController topViewController];
        
        //create a set containing the current background color of the top view controller
        NSMutableSet *usedColorSet = [NSMutableSet setWithObject:vc.view.backgroundColor];
        
        //create a set containing all the colors provided
        UIColor *blueColor = [UIColor colorWithRed:(23.0f/255.0f) green:(112.0f/255.0f) blue:(162.0f/255.0f) alpha:1.0f];
        UIColor *yellowColor = [UIColor colorWithRed:(255.0f/255.0f) green:(226.0f/255.0f) blue:(138.0f/255.0f) alpha:1.0f];
        UIColor *greenColor = [UIColor colorWithRed:(151.0f/255.0f) green:(192.0f/255.0f) blue:(152.0f/255.0f) alpha:1.0f];
        UIColor *pinkColor = [UIColor colorWithRed:(245.0f/255.0f) green:(112.0f/255.0f) blue:(109.0f/255.0f) alpha:1.0f];
        NSMutableSet *allColorsSet = [NSMutableSet setWithObjects:blueColor, yellowColor, greenColor, pinkColor, nil];
        
        //subtract the color of the top view controller from all the colors
        [allColorsSet minusSet:usedColorSet];
        
        //convert set into an array of usused colors
        NSMutableArray *unusedColorsArray = [[NSMutableArray alloc] initWithArray:[allColorsSet allObjects]];
        
        //get a random index from 0 to the number of objects in the array
        u_int32_t randomIndex = arc4random() % [unusedColorsArray count];
        
        //set the background color of the top view controller to a random color from the array
        vc.view.backgroundColor = [unusedColorsArray objectAtIndex:randomIndex];

        //store the assigned color in an object
        UIColor *shuffledTopViewColor = [unusedColorsArray objectAtIndex:randomIndex];

        //since the color at randomIndex is used, remove the object
        [unusedColorsArray removeObjectAtIndex:randomIndex];

        //convert the usedColorSet into an array and get the last and only element
        NSArray *colors = [usedColorSet allObjects];

        //add the previously used, but now unused color to the unusedColors array
        [unusedColorsArray addObject:[colors lastObject]];
        
        //create dictionaries with key as a name for the view controller
        //and value as an archived object converted to NSData. Required, since NSUserDefaults does
        //not allow to store UIColor objects
        NSDictionary *colorDict = nil;
        
        if([vc isKindOfClass:[HomeViewController class]]) {
            colorDict = @{@"home":[NSKeyedArchiver archivedDataWithRootObject:shuffledTopViewColor],
                          @"alarm":[NSKeyedArchiver archivedDataWithRootObject:[unusedColorsArray objectAtIndex:0]],
                          @"weather":[NSKeyedArchiver archivedDataWithRootObject:[unusedColorsArray objectAtIndex:1]],
                          @"watch":[NSKeyedArchiver archivedDataWithRootObject:[unusedColorsArray objectAtIndex:2]],
                          @"quote":[NSKeyedArchiver archivedDataWithRootObject:[unusedColorsArray objectAtIndex:1]]
                          };
        }
        else if ([vc isKindOfClass:[WatchDesignsContentViewController class]]) {
            colorDict = @{@"watch":[NSKeyedArchiver archivedDataWithRootObject:shuffledTopViewColor],
                          @"quote":[NSKeyedArchiver archivedDataWithRootObject:[unusedColorsArray objectAtIndex:0]],
                          @"weather":[NSKeyedArchiver archivedDataWithRootObject:[unusedColorsArray objectAtIndex:1]],
                          @"alarm":[NSKeyedArchiver archivedDataWithRootObject:[unusedColorsArray objectAtIndex:2]],
                          @"home":[NSKeyedArchiver archivedDataWithRootObject:[unusedColorsArray objectAtIndex:1]]
                          };
            
        }
        else if ([vc isKindOfClass:[AlarmViewController class]]) {
            colorDict = @{@"alarm":[NSKeyedArchiver archivedDataWithRootObject:shuffledTopViewColor],
                          @"quote":[NSKeyedArchiver archivedDataWithRootObject:[unusedColorsArray objectAtIndex:0]],
                          @"watch":[NSKeyedArchiver archivedDataWithRootObject:[unusedColorsArray objectAtIndex:1]],
                          @"weather":[NSKeyedArchiver archivedDataWithRootObject:[unusedColorsArray objectAtIndex:2]],
                          @"home":[NSKeyedArchiver archivedDataWithRootObject:[unusedColorsArray objectAtIndex:1]]
                          };
            
        }
        else if ([vc isKindOfClass:[DailyQuoteViewController class]]) {
            colorDict = @{@"quote":[NSKeyedArchiver archivedDataWithRootObject:shuffledTopViewColor],
                          @"weather":[NSKeyedArchiver archivedDataWithRootObject:[unusedColorsArray objectAtIndex:0]],
                          @"watch":[NSKeyedArchiver archivedDataWithRootObject:[unusedColorsArray objectAtIndex:1]],
                          @"home":[NSKeyedArchiver archivedDataWithRootObject:[unusedColorsArray objectAtIndex:2]],
                          @"alarm":[NSKeyedArchiver archivedDataWithRootObject:[unusedColorsArray objectAtIndex:1]]
                          };
            
        }
        else if ([vc isKindOfClass:[WeatherViewController class]]) {
            colorDict = @{@"weather":[NSKeyedArchiver archivedDataWithRootObject:shuffledTopViewColor],
                          @"quote":[NSKeyedArchiver archivedDataWithRootObject:[unusedColorsArray objectAtIndex:0]],
                          @"alarm":[NSKeyedArchiver archivedDataWithRootObject:[unusedColorsArray objectAtIndex:1]],
                          @"home":[NSKeyedArchiver archivedDataWithRootObject:[unusedColorsArray objectAtIndex:2]],
                          @"watch":[NSKeyedArchiver archivedDataWithRootObject:[unusedColorsArray objectAtIndex:1]]
                          };
            
        }
        //save the current date. From this date 24 hours onwards, the color will change again
        [[NSUserDefaults standardUserDefaults] setObject:[[NSDate alloc] init] forKey:@"previousDate"];
        //insert the color dictionary object
        [[NSUserDefaults standardUserDefaults] setObject:colorDict forKey:@"colorsDict"];
        
        //save user defaults
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (void)applicationWillTerminate:(UIApplication *)application {
	// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
	// Saves changes in the application's managed object context before the application terminates.
	[self saveContext];
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.zensarDev.WidgetDemo" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"AlarmModel" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"AlarmModel.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    
    if (application.applicationState == UIApplicationStateInactive || application.applicationState == UIApplicationStateActive || application.applicationState == UIApplicationStateBackground) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        NotificationTappedViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"NotificationTappedViewController"];
        viewController.firedNotification = notification;
        [self.window.rootViewController presentViewController:viewController animated:YES completion:nil];
    }
    
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Alarm!" message:notification.alertBody delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Snooze", nil];
//    firedNotification = notification;
//    NSLog(@"%ld", [[[UIApplication sharedApplication] scheduledLocalNotifications] count]);
//    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0:
            [[UIApplication sharedApplication] cancelLocalNotification:firedNotification];
            break;
        case 1: {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            NewAlarmViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"NewAlarmViewController"];
            [vc snoozeNotification:firedNotification];
            break;
        }
        default:
            break;
    }
}


@end
