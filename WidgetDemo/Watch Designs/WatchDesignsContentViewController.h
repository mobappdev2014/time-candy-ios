//
//  WatchDesignsContentViewController.h
//  WidgetDemo
//
//  Created by Salil Shahane on 29/12/14.
//  Copyright (c) 2014 Zensar Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClockView.h"

@interface WatchDesignsContentViewController : UIViewController

@property (strong, nonatomic) NSString *minuteHandImageName;
@property (strong, nonatomic) NSString *hourHandImageName;
@property (strong, nonatomic) NSString *clockFaceImageName;
@property (strong, nonatomic) IBOutlet ClockView *clockView;
@property NSUInteger pageIndex;

@end
