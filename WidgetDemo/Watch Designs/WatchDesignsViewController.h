//
//  WatchDesignsViewController.h
//  WidgetDemo
//
//  Created by Salil Shahane on 29/12/14.
//  Copyright (c) 2014 Zensar Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WatchDesignsContentViewController.h"
#import "BaseViewController.h"

@interface WatchDesignsViewController : BaseViewController <UIPageViewControllerDataSource, UIActionSheetDelegate>

- (WatchDesignsContentViewController *)viewControllerAtIndex:(NSUInteger)index;
@property (weak, nonatomic) IBOutlet UIButton *btnPrevious;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet GADBannerView *bannerView;

@end
