//
//  WatchDesignsContentViewController.m
//  WidgetDemo
//
//  Created by Salil Shahane on 29/12/14.
//  Copyright (c) 2014 Zensar Technologies. All rights reserved.
//

#import "WatchDesignsContentViewController.h"

@interface WatchDesignsContentViewController () {
    NSDictionary *selectedSkin;
}

@end

@implementation WatchDesignsContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //set the clockview images
    [_clockView setClockBackgroundImage:[UIImage imageNamed:_clockFaceImageName].CGImage];
    [_clockView setHourHandImage:[UIImage imageNamed:_hourHandImageName].CGImage];
    [_clockView setMinHandImage:[UIImage imageNamed:_minuteHandImageName].CGImage];
    [self.view addSubview:_clockView];
    [_clockView start];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
