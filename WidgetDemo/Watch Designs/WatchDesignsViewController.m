//
//  WatchDesignsViewController.m
//  WidgetDemo
//
//  Created by Salil Shahane on 29/12/14.
//  Copyright (c) 2014 Zensar Technologies. All rights reserved.
//

#import "WatchDesignsViewController.h"
#import "HomeViewController.h"
#import "AlarmViewController.h"
#import "WeatherViewController.h"
#import "DailyQuoteViewController.h"
#import "Common.h"
#import "AboutViewController.h"
#import "SettingsViewController.h"

@interface WatchDesignsViewController () {
    NSDictionary *selectedSkin;
}

@property BOOL didPressButton;
@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSArray *clockFaces;
@property (strong, nonatomic) NSArray *minuteHands;
@property (strong, nonatomic) NSArray *hourHands;
@property NSUInteger pageIndex;
- (IBAction)homeButtonPressed:(id)sender;
@end

@implementation WatchDesignsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //load ads using Google AdMob SDK
    [self loadAds];    
    
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    
    //don't set dataSource property to disable swipe gesture navigation
    //self.pageViewController.dataSource = self;
    
    #ifdef pro
    _clockFaces = @[@"CH1b", @"Clock15-400-Dial", @"Clock16-400-Dial", @"Clock17-400-Dial", @"Clock18-400-Dial", @"Clock19-400-Dial", @"Clock20-400-Dial", @"Clock21-400-Dial", @"Clock23-400-Dial", @"Clock24-400-Dial", @"Clock25-400-Dial.png", @"Clock26-400-Dial.png", @"Clock27-400-Dial", @"clock001-facet.png", @"clock002-facet.png", @"clock003-facet.png", @"clock004-facet.png",@"clock005-facet.png",@"clock006-facet.png", @"clock007-facet.png",@"clock008-facet.png",@"clock009-facet.png",@"clock0011-facet.png",@"clock0012-facet.png",@"clock0013-facet.png",@"clock0014-facet.png"];
   
    _minuteHands = @[@"iOS2", @"Clock15-IOS-Min", @"Clock16-IOS-Min", @"Clock17-IOS-Min", @"Clock18-IOS-Min", @"Clock19-IOS-Min", @"Clock20-IOS-Min", @"Clock21-IOS-Min", @"Clock23-IOS-Min", @"Clock24-IOS-Min", @"Clock25-IOS-Min", @"Clock26-IOS-Min", @"Clock27-IOS-Min", @"clock001-min.png", @"clock002-min.png", @"clock003-min.png", @"clock004-min.png", @"clock005-min.png", @"clock006-min.png", @"clock007-min.png", @"clock008-min.png", @"clock009-min.png", @"clock0011-min.png", @"clock0012-min.png", @"clock0013-min.png", @"clock0014-min.png"];
    
    _hourHands = @[@"iOS1", @"Clock15-IOS-HR", @"Clock16-IOS-HR", @"Clock17-IOS-HR", @"Clock18-IOS-HR", @"Clock19-IOS-HR", @"Clock20-IOS-HR", @"Clock21-IOS-HR", @"Clock23-IOS-HR", @"Clock24-IOS-HR", @"Clock25-IOS-HR.png", @"Clock26-IOS-HR.png", @"Clock27-IOS-HR.png", @"clock001-hr.png", @"clock002-hr.png", @"clock003-hr.png", @"clock004-hr.png", @"clock005-hr.png", @"clock006-hr.png", @"clock007-hr.png", @"clock008-hr.png", @"clock009-hr.png", @"clock0011-hr.png", @"clock0012-hr.png", @"clock0013-hr.png", @"clock0014-hr.png", ];
    #else
    _clockFaces = @[@"CH1b", @"Clock15-400-Dial", @"Clock16-400-Dial", @"Clock18-400-Dial", @"Clock19-400-Dial", @"Clock23-400-Dial", @"Clock24-400-Dial", @"Clock25-400-Dial.png", @"Clock27-400-Dial", @"clock002-facet.png", @"clock003-facet.png", @"clock005-facet.png", @"clock006-facet.png", @"clock007-facet.png", @"clock009-facet.png", @"clock0013-facet.png"];
    
    _minuteHands = @[@"iOS2", @"Clock15-IOS-Min", @"Clock16-IOS-Min", @"Clock18-IOS-Min", @"Clock19-IOS-Min", @"Clock23-IOS-Min", @"Clock24-IOS-Min", @"Clock25-IOS-Min", @"Clock27-IOS-Min", @"clock002-min.png", @"clock003-min.png", @"clock005-min.png", @"clock006-min.png", @"clock007-min.png", @"clock009-min.png", @"clock0013-min.png"];
    
    _hourHands = @[@"iOS1", @"Clock15-IOS-HR", @"Clock16-IOS-HR", @"Clock18-IOS-HR", @"Clock19-IOS-HR", @"Clock23-IOS-HR", @"Clock24-IOS-HR", @"Clock25-IOS-HR.png", @"Clock27-IOS-HR.png", @"clock002-hr.png", @"clock003-hr.png", @"clock005-hr.png", @"clock006-hr.png", @"clock007-hr.png", @"clock009-hr.png", @"clock0013-hr.png"];
    #endif
    
    WatchDesignsContentViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    // Change the size of page view controller
    self.pageViewController.view.frame = CGRectMake(0, 0, 220, 220);
    self.pageViewController.view.backgroundColor = [UIColor clearColor];
    [self.pageViewController.view setCenter:self.view.center];
    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
    [_btnPrevious addTarget:self action:@selector(btnPreviousPressed:) forControlEvents:UIControlEventTouchUpInside];
    [_btnNext addTarget:self action:@selector(btnNextPressed:) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSDictionary *colorsDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"colorsDict"];
    if([[colorsDict allKeys] count] > 0) {
        NSData *colorData = [colorsDict objectForKey:@"watch"];
        if(colorData) {
            UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
            self.view.backgroundColor = color;
        }
    }
}

- (IBAction)btnPreviousPressed:(id)sender {
    _didPressButton = YES;
    if(_pageIndex > 0)
    [self.pageViewController setViewControllers:@[[self viewControllerAtIndex:_pageIndex--]] direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:nil];
}

- (IBAction)btnNextPressed:(id)sender {
     _didPressButton = YES;
    if(_pageIndex < [_hourHands count] - 1)
        [self.pageViewController setViewControllers:@[[self viewControllerAtIndex:_pageIndex++]] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (WatchDesignsContentViewController *)viewControllerAtIndex:(NSUInteger)index
{
    // Create a new view controller and pass suitable data.
    if(_didPressButton)
        index = _pageIndex;
    
    WatchDesignsContentViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WatchDesignsContentViewController"];
    pageContentViewController.hourHandImageName = self.hourHands[index];
    pageContentViewController.minuteHandImageName = self.minuteHands[index];
    pageContentViewController.clockFaceImageName = self.clockFaces[index];
    pageContentViewController.pageIndex = index;
    return pageContentViewController;
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    _didPressButton = NO;
    NSUInteger index = ((WatchDesignsContentViewController*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    _didPressButton = NO;
    NSUInteger index = ((WatchDesignsContentViewController*) viewController).pageIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
        
    if (index == [self.hourHands count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

- (IBAction)homeButtonPressed:(id)sender {
    [self navigateToVC:sender];
}

- (IBAction)btnSetPressed:(id)sender {
    
    selectedSkin = @{@"hourHand":_hourHands[_pageIndex],
                     @"clockFace":_clockFaces[_pageIndex],
                     @"minuteHand":_minuteHands[_pageIndex]};
    
    NSUserDefaults *sharedDefaults = nil;
    
    #ifndef pro
        sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.Beetrisa.TimeCandy"];
        [sharedDefaults setObject:selectedSkin forKey:@"selectedSkin"];
    #else
        sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.Beetrisa.TimeCandyPRO"];
        [sharedDefaults setObject:selectedSkin forKey:@"selectedSkin"];
    #endif
    [sharedDefaults synchronize];
    NSLog(@"%@", [sharedDefaults dictionaryRepresentation]);
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Common getApplicationName] message:@"Selected watch successfully set as your widget !" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alert show];
}

- (IBAction)btnShareToFacebookPressed:(id)sender {
    [Common shareOnFacebookWithMessage:nil];
}

- (IBAction)btnShareToTwitterPressed:(id)sender {
    [Common shareOnTwitterWithMessage:nil];
}

- (IBAction)btnShareToLinkedInPressed:(id)sender {
    [Common authenticateLinkedInUserWithMessage:nil];
}

- (IBAction)btnMorePressed:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Please select an option" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"About", @"Settings", nil];
    [actionSheet showInView:self.view];
}

#pragma mark - UIActionSheet Delegate Method

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0: {
            AboutViewController *aboutVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutViewController"];
            [self.navigationController pushViewController:aboutVC animated:YES];
            break;
        }
        case 1: {
            SettingsViewController *settingsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
            [self.navigationController pushViewController:settingsVC animated:YES];
            break;
        }
        default:
            break;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
