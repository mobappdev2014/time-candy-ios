//
//  NewAlarmViewController.m
//  Awake
//
//  Created by Eliot Fowler on 7/18/13.
//  Copyright (c) 2013 Eliot Fowler. All rights reserved.
//

#import "NewAlarmViewController.h"
#import "RepeatViewController.h"
#import "LabelViewController.h"
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "AlarmData.h"
#import "Alarm.h"

@interface NewAlarmViewController () <UITableViewDataSource, UITableViewDelegate, UINavigationBarDelegate, UINavigationControllerDelegate, UIActionSheetDelegate>
@property (weak, nonatomic) IBOutlet UITableView *alarmSettingsTableView;
@property (strong, nonatomic) AlarmData* myAlarm;

@property (strong, nonatomic) NSDate *alarmTime;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSMutableDictionary *repeatDays;
@property (strong, nonatomic) NSString *repeatString;
@property (strong, nonatomic) NSString *sound;
@property (strong, nonatomic) NSString *snoozeDuration;
@property (strong, nonatomic) NSIndexPath *selectedIndexPath;
@property (strong, nonatomic) UISwitch *toggleSwitch;
@property BOOL isSet;
@property BOOL didSnooze;

@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property bool savedOrCancelled;

@end

@implementation NewAlarmViewController



- (id)initWithCoder:(NSCoder *)aDecoder
{
	if (self = [super initWithCoder:aDecoder]) {
		NSLog(@"%s", __PRETTY_FUNCTION__);
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	
    [self loadAds];
	//	UIBarButtonItem *buttonCancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(didClickCancel:)];
	//	[self.navigationItem setLeftBarButtonItem:buttonCancel];
    
	UIBarButtonItem *buttonSave = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(didClickSave:)];
	[self.navigationItem setRightBarButtonItem:buttonSave];
	
	_savedOrCancelled = false;
	[_datePicker setDatePickerMode:UIDatePickerModeTime];
	_alarmTime = _datePicker.date;
	self.title = @"Alarm";
	_repeatDays = [[NSMutableDictionary alloc]
				   initWithObjectsAndKeys:[NSNumber numberWithBool:NO], @"monday",
				   [NSNumber numberWithBool:NO], @"tuesday",
				   [NSNumber numberWithBool:NO], @"wednesday",
				   [NSNumber numberWithBool:NO], @"thursday",
				   [NSNumber numberWithBool:NO], @"friday",
				   [NSNumber numberWithBool:NO], @"saturday",
				   [NSNumber numberWithBool:NO], @"sunday",
				   nil];
	_sound = @"Alarm";
	_isSet = YES;
}

- (void)viewDidAppear:(BOOL)animated {
	if(_savedOrCancelled) {
		_savedOrCancelled = false;
		_datePicker.date = [NSDate date];
		_alarmTime = _datePicker.date;
		self.title = @"Alarm";
		_repeatDays = [[NSMutableDictionary alloc]
					   initWithObjectsAndKeys:[NSNumber numberWithBool:NO], @"monday",
					   [NSNumber numberWithBool:NO], @"tuesday",
					   [NSNumber numberWithBool:NO], @"wednesday",
					   [NSNumber numberWithBool:NO], @"thursday",
					   [NSNumber numberWithBool:NO], @"friday",
					   [NSNumber numberWithBool:NO], @"saturday",
					   [NSNumber numberWithBool:NO], @"sunday",
					   nil];
		_sound = @"Alarm";
		_isSet = YES;
	}
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return 5;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if(indexPath.row == 0) {
		UITableViewCell *settingsCell = [tableView dequeueReusableCellWithIdentifier:@"settingsCell"];
		if (settingsCell == nil) {
			settingsCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"settingsCell"];
		}
		settingsCell.textLabel.text = @"Repeat";
		settingsCell.detailTextLabel.text = @"Never";
		settingsCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
		[settingsCell setBackgroundColor:[UIColor clearColor]];
		
		return settingsCell;
	} else if(indexPath.row == 1) {
		UITableViewCell *settingsCell = [tableView dequeueReusableCellWithIdentifier:@"settingsCell"];
		if (settingsCell == nil) {
			settingsCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"settingsCell"];
		}
		settingsCell.textLabel.text = @"Alarm Text";
		settingsCell.detailTextLabel.text = @"Alarm";
		settingsCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
		[settingsCell setBackgroundColor:[UIColor clearColor]];
		
		return settingsCell;
	} else if(indexPath.row == 2) {
		UITableViewCell *settingsCell = [tableView dequeueReusableCellWithIdentifier:@"settingsCell"];
		if (settingsCell == nil) {
			settingsCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"settingsCell"];
		}
		settingsCell.textLabel.text = @"Sound";
		settingsCell.detailTextLabel.text = _sound;
		settingsCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
		[settingsCell setBackgroundColor:[UIColor clearColor]];
		
		return settingsCell;
	} else if(indexPath.row == 3) {
		UITableViewCell *snoozeCell = [tableView dequeueReusableCellWithIdentifier:@"snoozeCell"];
		if (snoozeCell == nil) {
			snoozeCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"snoozeCell"];
		}
		
		_toggleSwitch = [[UISwitch alloc] init];
		snoozeCell.accessoryView = [[UIView alloc] initWithFrame:_toggleSwitch.frame];
		[snoozeCell.accessoryView addSubview:_toggleSwitch];
		[_toggleSwitch addTarget:self action:@selector(didChangeSwitch:) forControlEvents:UIControlEventValueChanged];
		[snoozeCell setBackgroundColor:[UIColor clearColor]];
		
		snoozeCell.textLabel.text = @"Snooze";
		
		return snoozeCell;
	} else if(indexPath.row == 4) {
		UITableViewCell *settingsCell = [tableView dequeueReusableCellWithIdentifier:@"settingsCell"];
		if (settingsCell == nil) {
			settingsCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"settingsCell"];
		}
		settingsCell.textLabel.text = @"Duration (in minutes)";
		settingsCell.detailTextLabel.text = _snoozeDuration;
		settingsCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
		if(![_toggleSwitch isOn])
			settingsCell.hidden = YES;
		else
			settingsCell.hidden = NO;
		//settingsCell.indentationLevel = 2;
		[settingsCell setBackgroundColor:[UIColor clearColor]];
		
		return settingsCell;
	}
	
	return nil;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if([segue.identifier isEqualToString:@"showRepeatView"]){
		RepeatViewController *controller = (RepeatViewController *)segue.destinationViewController;
		controller.repeatDays = _repeatDays;
		controller.delegate = self;
	} else if([segue.identifier isEqualToString:@"showLabelView"]){
		LabelViewController *controller = (LabelViewController *)segue.destinationViewController;
		controller.delegate = self;
	}
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	_selectedIndexPath = indexPath;
	NSLog(@"You clicked on row %ld: %@",(long)[indexPath row], [[[tableView cellForRowAtIndexPath:indexPath] textLabel] text]);
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	if ([indexPath row] == 0) {
		RepeatViewController *repeatVC = [self.storyboard instantiateViewControllerWithIdentifier:@"RepeatViewController"];
		repeatVC.delegate = self;
		[self.navigationController pushViewController:repeatVC animated:YES];
	}
	if ([indexPath row] == 1) {
		LabelViewController *labelVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LabelViewController"];
		labelVC.delegate = self;
		[self.navigationController pushViewController:labelVC animated:YES];
	}
	if ([indexPath row] == 2) {
		AlarmSoundsViewController *alarmSoundsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AlarmSoundsViewController"];
		alarmSoundsVC.delegate = self;
		[self.navigationController pushViewController:alarmSoundsVC animated:YES];
	}
	if(indexPath.row == 4) {
		UIActionSheet *snoozeActionSheet = [[UIActionSheet alloc] initWithTitle:@"Select snooze duration" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"5", @"10", @"15", @"20", @"25", @"30", nil];
		[snoozeActionSheet showInView:self.view];
		_selectedIndexPath = indexPath;
	}
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	
	_snoozeDuration = [actionSheet buttonTitleAtIndex:buttonIndex];
	[[NSUserDefaults standardUserDefaults] setObject:_snoozeDuration forKey:@"snoozeDuration"];
	[[NSUserDefaults standardUserDefaults] synchronize];
	[_alarmSettingsTableView reloadRowsAtIndexPaths:@[_selectedIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (IBAction)didChangeSwitch:(UISwitch*)sender {
	UITableViewCell *cell = [_alarmSettingsTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:0]];
	if(sender.on) {
		cell.hidden = NO;
	} else {
		cell.hidden = YES;
	}
}

- (IBAction)didClickCancel:(id)sender
{
	_savedOrCancelled = true;
    [self.navigationController popViewControllerAnimated:YES];
	//[[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)didClickSave:(id)sender
{
	AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [appDelegate managedObjectContext];
	NSManagedObject *newAlarm = [NSEntityDescription
								 insertNewObjectForEntityForName:@"Alarm"
								 inManagedObjectContext:context];
	
	//Here we need to get rid of the date portion of the date
//	NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
//	NSDateComponents *comps = [calendar components:(NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit) fromDate:_alarmTime];
//	NSDate *timeWithoutDate = [calendar dateFromComponents:comps];
	
	[newAlarm setValue:_alarmTime forKey:@"date"];
	[newAlarm setValue:self.title forKey:@"title"];
	NSNumber *isSet = [NSNumber numberWithBool:_isSet];
	[newAlarm setValue:isSet forKey:@"isSet"];
	[newAlarm setValue:_sound forKey:@"sound"];
	[newAlarm setValue:_repeatString forKey:@"repeatString"];
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [newAlarm setValue:[numberFormatter numberFromString:_snoozeDuration] forKey:@"snoozeLength"];
    
	NSError *error;
    if ([context save:&error]) {
        [self scheduleLocalNotification];
    }
	[self.navigationController popViewControllerAnimated:YES];
}

-(void)didFinishEditingLabel:(NSString *)label
{
	self.title = label;
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
	UITableViewCell *labelCell = [_alarmSettingsTableView cellForRowAtIndexPath:indexPath];
	labelCell.detailTextLabel.text = label;
}

-(void)didSetRepeatDays:(NSMutableDictionary *)repeatDays withRepeatString:(NSString *)repeatString
{
	_repeatDays = repeatDays;
	_repeatString = repeatString;
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
	UITableViewCell *repeatCell = [_alarmSettingsTableView cellForRowAtIndexPath:indexPath];
	repeatCell.detailTextLabel.text = _repeatString;
}

#pragma mark - UINavigatonBarDelegate

- (UIBarPosition)positionForBar:(id<UIBarPositioning>)bar
{
	return UIBarPositionTopAttached;
}
- (IBAction)didChangePickerValue:(id)sender {
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
	NSDate *selectedDate = [sender date];
	NSString *strSelectedDate = [dateFormatter stringFromDate:selectedDate];
	_alarmTime = [dateFormatter dateFromString:strSelectedDate];
}

- (NSDate *)nextDate:(NSInteger)dayofWeek {
    NSCalendar *gregorian = [NSCalendar autoupdatingCurrentCalendar];
    [gregorian setLocale:[NSLocale currentLocale]];
    
    NSDateComponents *nowComponents = [gregorian components:NSYearCalendarUnit | NSWeekCalendarUnit |  NSMonthCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:_alarmTime];
    
    [nowComponents setWeekday:dayofWeek]; //Monday
    [nowComponents setWeekOfMonth:[nowComponents weekOfMonth] + 1];
    NSDate *beginningOfWeek = [gregorian dateFromComponents:nowComponents];
    return beginningOfWeek;
}

#pragma mark - private helper methods
- (void)scheduleLocalNotification {
    
	UILocalNotification* localNotification = [[UILocalNotification alloc] init];
	NSDate* today = [NSDate date];
	// We need to schedule a local notification
	bool neverRepeats = true;
	for(id key in _repeatDays) {
		if([[_repeatDays objectForKey:key] boolValue]) {
			neverRepeats = false;
            
            if([key isEqualToString:@"sunday"])
                localNotification.fireDate = [self nextDate:1];
            
            if([key isEqualToString:@"monday"])
                localNotification.fireDate = [self nextDate:2];
            
            if([key isEqualToString:@"tuesday"])
                localNotification.fireDate = [self nextDate:3];
            
            if([key isEqualToString:@"wednesday"])
                localNotification.fireDate = [self nextDate:4];
            
            if([key isEqualToString:@"thursday"])
                localNotification.fireDate = [self nextDate:5];
            
            if([key isEqualToString:@"friday"])
                localNotification.fireDate = [self nextDate:6];
            
            if([key isEqualToString:@"saturday"])
                localNotification.fireDate = [self nextDate:7];
            
			localNotification.alertBody = self.title;
			localNotification.timeZone = [NSTimeZone localTimeZone];
			localNotification.alertAction = @"I'm up, I'm up!";

            //if(![_sound isEqualToString:@"Default"] || ![_sound isEqualToString:@""] || ![_sound isEqualToString:@"Alarm"])
                localNotification.soundName = [NSString stringWithFormat:@"%@.m4a", _sound];
           //else
               // localNotification.soundName = UILocalNotificationDefaultSoundName;
            
			localNotification.repeatInterval = NSWeekCalendarUnit;
            
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
            NSEntityDescription *entity = [NSEntityDescription entityForName:@"Alarm" inManagedObjectContext:appDelegate.managedObjectContext];
            [fetchRequest setEntity:entity];
            
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:YES];
            NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
            [fetchRequest setSortDescriptors:sortDescriptors];
            
            NSError *error = nil;
            NSArray *alarms = [appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
            Alarm *alarm = [alarms lastObject];
            NSLog(@"%@", [[alarm.objectID URIRepresentation] lastPathComponent]);
			localNotification.userInfo = @{@"alarmObj":[[alarm.objectID URIRepresentation] absoluteString]};
			[[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
		}
	}
	if(neverRepeats) {
		if([_alarmTime earlierDate:today] == today) {
			localNotification.fireDate = _alarmTime;
			localNotification.alertBody = @"Title";
			localNotification.timeZone = [NSTimeZone localTimeZone];
            //if(![_sound isEqualToString:@"Default"] || ![_sound isEqualToString:@""] || ![_sound isEqualToString:@"Alarm"])
                localNotification.soundName = [NSString stringWithFormat:@"%@.m4a", _sound];
//            else
//                localNotification.soundName = UILocalNotificationDefaultSoundName;
			localNotification.alertAction = @"I'm up, I'm up!";
            
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
            NSEntityDescription *entity = [NSEntityDescription entityForName:@"Alarm" inManagedObjectContext:appDelegate.managedObjectContext];
            [fetchRequest setEntity:entity];
            
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:YES];
            NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
            [fetchRequest setSortDescriptors:sortDescriptors];
            
            NSError *error = nil;
            NSArray *alarms = [appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
            Alarm *alarm = [alarms lastObject];
            NSLog(@"%@", [[alarm.objectID URIRepresentation] lastPathComponent]);
            localNotification.userInfo = @{@"alarmObj":[[alarm.objectID URIRepresentation] absoluteString]};
            
			[[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
		}
	}
}

- (void)scheduleLocalNotificationForSelectedTime {
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = _alarmTime;
    localNotification.timeZone = [NSTimeZone localTimeZone];
    localNotification.alertBody = self.title;
    localNotification.alertAction = @"I'm up, I'm up!";

    localNotification.soundName = [NSString stringWithFormat:@"%@.m4a", _sound];
    localNotification.repeatInterval = NSWeekCalendarUnit;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Alarm" inManagedObjectContext:appDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSError *error = nil;
    NSArray *alarms = [appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    Alarm *alarm = [alarms lastObject];
    NSLog(@"%@", [[alarm.objectID URIRepresentation] lastPathComponent]);
    localNotification.userInfo = @{@"alarmObj":[[alarm.objectID URIRepresentation] absoluteString]};
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

- (void)snoozeNotification:(UILocalNotification *)localNotification {
	
	NSLog(@"snooze in secs : %ld", [[NSUserDefaults standardUserDefaults] integerForKey:@"snoozeDuration"] * 60);
	NSLog(@"prev alarm time : %@", [[NSUserDefaults standardUserDefaults]objectForKey:@"alarmTime"]);
	NSLog(@"next fire date : %@", [NSDate dateWithTimeInterval:[[NSUserDefaults standardUserDefaults] integerForKey:@"snoozeDuration"] * 60 sinceDate:[[NSDate alloc] init]]);
	
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSDate *selectedDate = [[NSDate alloc] init];
    NSString *strSelectedDate = [dateFormatter stringFromDate:selectedDate];
    NSDate *currentDate = [dateFormatter dateFromString:strSelectedDate];
    
    NSDate *nextDate = [NSDate dateWithTimeInterval:[[NSUserDefaults standardUserDefaults] integerForKey:@"snoozeDuration"] * 60 sinceDate:currentDate];
	
	localNotification.fireDate = nextDate;
	[[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

- (void)selectedAlarmSound:(NSString *)filePath {
	_sound = filePath;
	[self.alarmSettingsTableView reloadRowsAtIndexPaths:@[_selectedIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

@end
