//
//  Common.m
//  WidgetDemo
//
//  Created by Mitesh Bhadrecha on 11/12/14.
//  Copyright (c) 2014 Zensar Technologies. All rights reserved.
//

#import "Common.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import "LIALinkedInHttpClient.h"
#import "LIALinkedInApplication.h"
#import "LIALinkedInAuthorizationViewController.h"

@implementation Common

//gets the application name from the Bundle Name property in the plist file
+ (NSString *)getApplicationName {
	
	return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleName"];
}

//shows a loader on the specified view
/*
 @param baseView - the view on which the loader is to be shown
 @param style - the style of the loader to be shown
*/
- (void)showLoaderOnView:(UIView *)baseView withStyle:(UIActivityIndicatorViewStyle)style {
    
    _activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:style];
    [_activityIndicatorView setFrame:CGRectMake(baseView.bounds.size.width / 2, baseView.bounds.size.height / 2, 25, 25)];
    [_activityIndicatorView setHidden:NO];
    [_activityIndicatorView startAnimating];
    [baseView addSubview:_activityIndicatorView];
}

//hides the loader
- (void)hideLoader {
    [_activityIndicatorView stopAnimating];
    [_activityIndicatorView setHidden:YES];
}

//shares a predefined message on twitter
/*
 @param message - if sharing from the daily quotes view controller, the message will be the current 
 quote on the screen. If sharing from other views, a default message will be shared.
*/
+ (void)shareOnTwitterWithMessage:(NSString *)message {

    __block UIAlertView *alert = nil;
    ACAccountStore *account = [[ACAccountStore alloc] init];
    ACAccountType *accountType = [account accountTypeWithAccountTypeIdentifier:
                                  ACAccountTypeIdentifierTwitter];
    
    [account requestAccessToAccountsWithType:accountType options:nil
                                  completion:^(BOOL granted, NSError *error)
     {
         NSArray *arrayOfAccounts = [account
                                     accountsWithAccountType:accountType];
        
             if (granted == YES)
             {
                 if ([arrayOfAccounts count] > 0)
                 {

                    ACAccount *twitterAccount =
                     [arrayOfAccounts lastObject];
                     
                     NSDictionary *messageDict = nil;
                     
                     if([message length] > 0) {
                         messageDict = @{@"status": [NSString stringWithFormat:@"%@ http://beetrisa.in/clientele.html", message], @"wrap_links":@"true"};
                     }
                     else {
                         messageDict = @{@"status": @"Select and set a variety of beautifully designed and functional designer analog watches as your mobile wallpaper. Get weather updates http://beetrisa.in/clientele.html", @"wrap_links":@"true"};
                     }
                     
                     NSURL *requestURL = [NSURL URLWithString:@"https://api.twitter.com/1.1/statuses/update.json"];
                     
                     SLRequest *postRequest = [SLRequest
                                               requestForServiceType:SLServiceTypeTwitter
                                               requestMethod:SLRequestMethodPOST
                                               URL:requestURL parameters:messageDict];
                     
                     postRequest.account = twitterAccount;
                     
                     [postRequest
                      performRequestWithHandler:^(NSData *responseData,
                                                  NSHTTPURLResponse *urlResponse, NSError *error)
                      {
                          if(!error) {
                              alert = [[UIAlertView alloc] initWithTitle:[Common getApplicationName] message:@"Share Successful" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
                              NSLog(@"%ld", (long)[urlResponse statusCode]);
                              [alert performSelectorOnMainThread:@selector(show) withObject:alert waitUntilDone:NO];
                          }
                          else
                              alert = [[UIAlertView alloc] initWithTitle:[Common getApplicationName] message:[error localizedDescription] delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
                          [alert performSelectorOnMainThread:@selector(show) withObject:alert waitUntilDone:NO];
                      }];
                 }
                 else {
                     alert = [[UIAlertView alloc] initWithTitle:[Common getApplicationName] message:@"Please log in to Twitter via settings to share" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
                     [alert performSelectorOnMainThread:@selector(show) withObject:alert waitUntilDone:NO];
                 }
             }
             else {
                 alert = [[UIAlertView alloc] initWithTitle:[Common getApplicationName] message:@"Please grant access to Twitter via settings to share" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
                 [alert performSelectorOnMainThread:@selector(show) withObject:alert waitUntilDone:NO];
             }
     }];
}
//shares a predefined message on facebook
/*
 @param message - if sharing from the daily quotes view controller, the message will be the current
 quote on the screen. If sharing from other views, a default message will be shared.
 */
+ (void)shareOnFacebookWithMessage:(NSString *)message {
    
    __block UIAlertView *alert = nil;
    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
    
    __block ACAccount * facebookAccount;
    
    NSDictionary *emailReadPermisson = @{
                                         ACFacebookAppIdKey : @"1760316047526858",
                                         ACFacebookPermissionsKey : @[@"email"],
                                         ACFacebookAudienceKey : ACFacebookAudienceEveryone,
                                         };
    
    NSDictionary *publishWritePermisson = @{
                                            ACFacebookAppIdKey : @"1760316047526858",
                                            ACFacebookPermissionsKey : @[@"publish_actions"],
                                            ACFacebookAudienceKey : ACFacebookAudienceEveryone
                                            };
    
    ACAccountType *facebookAccountType = [accountStore
                                          accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    //Request for Read permission
    
    [accountStore requestAccessToAccountsWithType:facebookAccountType options:emailReadPermisson completion:^(BOOL granted, NSError *error) {
        
        if (granted && !error) {
            
            NSLog(@"Granted for read");
            
            //Request for write permission
            [accountStore requestAccessToAccountsWithType:facebookAccountType options:publishWritePermisson completion:^(BOOL granted, NSError *error) {
                
                if (granted && !error) {
                    
                    NSLog(@"Granted for post");
                    
                    NSArray *accounts = [accountStore
                                         accountsWithAccountType:facebookAccountType];
                    facebookAccount = [accounts lastObject];
                
                    if([accounts count] > 0) {
                        
                        ACAccount *facebookAccount = [accounts lastObject];
                        
                        SLRequest *postToMyWall = nil;
                        
                        NSURL *postURL = [NSURL URLWithString:@"https://graph.facebook.com/me/feed"];
                        
                        //NSString *picture = @"http://www.stuarticus.com/wp-content/uploads/2012/08/SDKsmall.png";
                        //                        NSString *name = @"Name";
                        //                        NSString *caption = @"Caption";
                        //                        NSString *description = @"Description";
                        
                        //@"name" : name,
                        //@"caption" : caption,
                        //@"description" : description,
                        
                        NSString *link = @"http://beetrisa.in/clientele.html";
                        NSDictionary *postDict = nil;
                        
                        if([message length] > 0) {
                        
                            postDict = @{
                                        @"link": link,
                                        @"message" : message,
                                        @"access_token":facebookAccount.credential.oauthToken
                                        };
                        }
                        else {
                            NSString *strDescription = @"Select and set a variety of beautifully designed and functional designer analog watches as your mobile wallpaper. Additional features of the app include: weather updates, quote of the day and alarm clock with a selection of musical tones.";

                            postDict = @{@"description" : strDescription,
                                         @"link": link,
                                         @"access_token":facebookAccount.credential.oauthToken
                                         };
                        }
                        
                        postToMyWall = [SLRequest requestForServiceType:SLServiceTypeFacebook requestMethod:SLRequestMethodPOST URL:postURL parameters:postDict];
                        
                        [postToMyWall setAccount:facebookAccount];
                        
                        [postToMyWall performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error)
                         {
                             if (error) {
                                 alert = [[UIAlertView alloc] initWithTitle:[Common getApplicationName] message:[error localizedDescription] delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
                                 [alert performSelectorOnMainThread:@selector(show) withObject:alert waitUntilDone:NO];
                             }
                             else
                             {
                                 NSLog(@"Post successful");
                                 NSString *dataString = [[NSString alloc] initWithData:responseData encoding:NSStringEncodingConversionAllowLossy];
                                 NSLog(@"Response Data: %@", dataString);
                                 alert = [[UIAlertView alloc] initWithTitle:[Common getApplicationName] message:@"Share Successful" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
                                 [alert performSelectorOnMainThread:@selector(show) withObject:alert waitUntilDone:NO];
                             }
                         }];
                    }
                    else {
                        alert = [[UIAlertView alloc] initWithTitle:[Common getApplicationName] message:@"Please log in to Facebook via settings to share" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
                        [alert performSelectorOnMainThread:@selector(show) withObject:alert waitUntilDone:NO];
                    }
                }
                else {
                    alert = [[UIAlertView alloc] initWithTitle:[Common getApplicationName] message:[error localizedDescription] delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
                    [alert performSelectorOnMainThread:@selector(show) withObject:alert waitUntilDone:NO];
                }
            }];
        }
        else {
            alert = [[UIAlertView alloc] initWithTitle:[Common getApplicationName] message:[error localizedDescription] delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
            [alert performSelectorOnMainThread:@selector(show) withObject:alert waitUntilDone:NO];
        }
    }];
}

//initializes an LIALinkedInHttpClient object for the library
+ (LIALinkedInHttpClient *)initializeLinkedInClient {
    
    LIALinkedInApplication *application = [LIALinkedInApplication applicationWithRedirectURL:@"http://www.beetrisa.in"
                                                                                    clientId:@"75l3hk8ox5gaof"
                                                                                clientSecret:@"CIL8Tl2biAQj0cuS"
                                                                                       state:@"DCEEFWF45453sdffef424"
                                                                               grantedAccess:@[@"r_fullprofile", @"r_network", @"rw_nus"]];
    
    LIALinkedInAuthorizationViewController *asd = [[LIALinkedInAuthorizationViewController alloc] initWithApplication:application success:^(NSString *code) {
        
    } cancel:^{
        
    } failure:^(NSError *errorReason) {
        
    }];
    
    return [LIALinkedInHttpClient clientForApplication:application presentingViewController:asd];
}

//if the user is not logged in, or does not have a valid token ; shows a login page.
//else calls postToLinkedInWithAccessToken helper method to post to linkedin
/*
 @param message - the daily quote to be passed to the potToLinkedInWithAccessToken method.
 Can be nil, if passed from other view controllers
*/
+ (void)authenticateLinkedInUserWithMessage:(NSString *)strMessage {
    
    __block UIAlertView *alert = nil;
    LIALinkedInHttpClient *client = [Common initializeLinkedInClient];
    if ([client validToken]) {
        [Common postToLinkedInWithAccessToken:[client accessToken] andMessage:strMessage withCompletionHandler:^(NSError *error) {
            if(error)
                alert = [[UIAlertView alloc] initWithTitle:[Common getApplicationName] message:[error domain] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            else
                alert = [[UIAlertView alloc] initWithTitle:[Common getApplicationName] message:@"Share Successful" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            
            [alert performSelectorOnMainThread:@selector(show) withObject:alert waitUntilDone:NO];
        }];
    }
    else {
        [client getAuthorizationCode:^(NSString *code) {
            [client getAccessToken:code success:^(NSDictionary *accessTokenData) {
                NSString *accessToken = [accessTokenData objectForKey:@"access_token"];
                
                [Common postToLinkedInWithAccessToken:accessToken andMessage:strMessage withCompletionHandler:^(NSError *error) {
                    if(error)
                        alert = [[UIAlertView alloc] initWithTitle:[Common getApplicationName] message:[error localizedDescription] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    else
                        alert = [[UIAlertView alloc] initWithTitle:[Common getApplicationName] message:@"Share Successful" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    
                    [alert performSelectorOnMainThread:@selector(show) withObject:alert waitUntilDone:NO];
                    
                }];
                
            }                   failure:^(NSError *error) {
                NSLog(@"Quering accessToken failed %@", error);
            }];
        }                      cancel:^{
            NSLog(@"Authorization was cancelled by user");
        }                     failure:^(NSError *error) {
            NSLog(@"Authorization failed %@", error);
            [Common initializeLinkedInClient];
        }];
    }
}

//helper method to create and send the post to linkedin
/*
 @param accessToken - an access token received when the user logs in
 @param message - the daily quote to be passed to the potToLinkedInWithAccessToken method.
 Can be nil, if passed from other view controllers
 @param completionHandler - a callback to handle success or error conditions
 */
+ (void)postToLinkedInWithAccessToken:(NSString *)accessToken andMessage:(NSString *)strMessage withCompletionHandler:(void (^) (NSError *error))completionHandler {
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://api.linkedin.com/v1/people/~/shares?oauth2_access_token=%@&format=json", accessToken]] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSDictionary *update = nil;
    if([strMessage length] > 0) {
    
        //add after visibility if needed - @"comment to share", @"comment",
        update = [[NSDictionary alloc] initWithObjectsAndKeys:
                                
                                [[NSDictionary alloc] initWithObjectsAndKeys: @"anyone",@"code",nil],  @"visibility",
                  
                                [[NSDictionary alloc] initWithObjectsAndKeys:strMessage, @"description",
                                 @"http://beetrisa.in/clientele.html", @"submittedUrl",
                                 @"Time Candy",@"title",
                                 @"image_url",@"submittedImageUrl",nil],
                                @"content",nil];
    }
    else {
        NSString *strDescription = @"Select and set a variety of beautifully designed and functional designer analog watches as your mobile wallpaper. Additional features of the app include: weather updates, quote of the day and alarm clock with a selection of musical tones.";
        update = [[NSDictionary alloc] initWithObjectsAndKeys:
                  
                  [[NSDictionary alloc] initWithObjectsAndKeys: @"anyone",@"code",nil],  @"visibility",
                  [[NSDictionary alloc] initWithObjectsAndKeys:strDescription, @"description",
                   @"http://beetrisa.in/clientele.html", @"submittedUrl",
                   @"Time Candy",@"title",
                   @"image_url",@"submittedImageUrl",nil],
                  @"content",nil];
    }
    
    NSError *error = nil;
    NSData *bodyData = [NSJSONSerialization dataWithJSONObject:update options:NSJSONWritingPrettyPrinted error:&error];
    
    [request setHTTPBody:bodyData];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    __block NSDictionary *respDict = nil;
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue new] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if(!connectionError) {
            respDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&connectionError];
            NSLog(@"%@", respDict);
            if([respDict objectForKey:@"updateKey"])
                completionHandler(nil);
            else
                completionHandler([NSError errorWithDomain:[respDict objectForKey:@"message"] code:[[respDict objectForKey:@"errorCode"] integerValue] userInfo:respDict]);
        }
        else {
            NSLog(@"%@", [connectionError localizedDescription]);
            completionHandler([NSError errorWithDomain:[respDict objectForKey:@"message"] code:[[respDict objectForKey:@"errorCode"] integerValue] userInfo:respDict]);
        }
    }];
    [[NSUserDefaults standardUserDefaults] setObject:accessToken forKey:@"linkedin_access_token"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

//shares a predefined message on whatsapp
+ (void)shareOnWhatsApp {

    //the message to be shared
    NSString *message = @"Select and set a variety of beautifully designed and functional designer analog watches as your mobile wallpaper. Additional features of the app include: weather updates, quote of the day and alarm clock with a selection of musical tones. http://beetrisa.in/clientele.html";
    
    NSString *urlWhats = [NSString stringWithFormat:@"whatsapp://send?text=%@", message];
    
    //url with spaces replaced by %20
    NSURL *whatsappURL = [NSURL URLWithString:[urlWhats stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    if([[UIApplication sharedApplication] canOpenURL:whatsappURL])
        [[UIApplication sharedApplication] openURL:whatsappURL];
    else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[Common getApplicationName] message:@"Please install WhatsApp to share" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
    }
}

//returns TRUE if 24 hours have elapsed since the previously saved date
+ (BOOL)hasDayElapsed {
    
    NSDate *previousDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"previousDate"];
    
    NSLog(@"%f", [previousDate timeIntervalSinceNow]);
    
    if([previousDate timeIntervalSinceNow] <= - 86400.0)
        
        return YES;
    
    return NO;
}

@end
