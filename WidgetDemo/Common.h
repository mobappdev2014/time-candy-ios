//
//  Common.h
//  WidgetDemo
//
//  Created by Mitesh Bhadrecha on 11/12/14.
//  Copyright (c) 2014 Zensar Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Common : NSObject <UIActionSheetDelegate>

@property (strong, nonatomic) UIActivityIndicatorView *activityIndicatorView;

+ (NSString *)getApplicationName;

- (void)showLoaderOnView:(UIView *)baseView withStyle:(UIActivityIndicatorViewStyle)style;

- (void)hideLoader;

+ (void)shareOnFacebookWithMessage:(NSString *)message;

+ (void)shareOnTwitterWithMessage:(NSString *)message;

+ (void)authenticateLinkedInUserWithMessage:(NSString *)strMessage;

+ (BOOL)hasDayElapsed;

+ (void)shareOnWhatsApp;

@end
