//
//  SettingsViewController.m
//  WidgetDemo
//
//  Created by Salil Shahane on 06/01/15.
//  Copyright (c) 2015 Zensar Technologies. All rights reserved.
//

#import "SettingsViewController.h"
#import "HelpViewController.h"
#import <Accounts/Accounts.h>
#import <Social/Social.h>
#import "Common.h"
#import "LikeWebViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    //load ads using Google AdMob SDK
    [self loadAds];
    
    self.navigationItem.title = @"Settings";
    
    //images and text for PRO and free versions
    #ifndef pro
        tableItems = @[@"LIKE", @"RATE", @"SHARE APP", @"UPGRADE", @"SUGGESTION", @"HELP"];
        tableImages = @[[UIImage imageNamed:@"ic_sett_like_light.png"], [UIImage imageNamed:@"ic_sett_rate_light.png"], [UIImage imageNamed:@"ic_sett_share_light.png"], [UIImage imageNamed:@"ic_sett_upgrade_light.png"], [UIImage imageNamed:@"ic_sett_sugg_light.png"], [UIImage imageNamed:@"ic_sett_help_light.png"]];
    #else
        tableItems = @[@"LIKE", @"RATE", @"SHARE APP", @"SUGGESTION", @"HELP"];
        tableImages = @[[UIImage imageNamed:@"ic_sett_like_light.png"], [UIImage imageNamed:@"ic_sett_rate_light.png"], [UIImage imageNamed:@"ic_sett_share_light.png"], [UIImage imageNamed:@"ic_sett_sugg_light.png"], [UIImage imageNamed:@"ic_sett_help_light.png"]];
    #endif
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

#pragma mark - UITableView DataSource Methods 

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [tableItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"settingsCell" forIndexPath:indexPath];

    UIImageView *imageView = (UIImageView *)[cell viewWithTag:1];
    UILabel *lbl = (UILabel *)[cell viewWithTag:2];
    
    imageView.image = [tableImages objectAtIndex:indexPath.row];
    lbl.text = [tableItems objectAtIndex:indexPath.row];
    
    return cell;
}

#pragma mark - UITableView Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    UILabel *cellLbl = (UILabel *)[cell.contentView viewWithTag:2];
    NSString *cellText = cellLbl.text;
    
    if([cellText isEqualToString:@"LIKE"]) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Please select an option" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Facebook", @"Twitter", @"LinkedIn", @"YouTube", @"Google +", @"Pinterest", nil];
        [actionSheet setTag:1001];
        [actionSheet showInView:self.view];
    }
    else if ([cellText isEqualToString:@"RATE"]) {
        LikeWebViewController *likeWebViewVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LikeWebViewController"];
        likeWebViewVC.strURL = @"http://beetrisa.in/clientele.html";
        [self.navigationController pushViewController:likeWebViewVC animated:YES];
        //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/keynote/id361285480?mt=8"]];
    }
    else if ([cellText isEqualToString:@"SHARE APP"]) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Please select an option" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Facebook", @"Twitter", @"LinkedIn", @"WhatsApp", @"Mail", @"SMS", nil];
        [actionSheet setTag:1000];
        [actionSheet showInView:self.view];
    }
    else if ([cellText isEqualToString:@"UPGRADE"]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[Common getApplicationName] message:@"UPGRADE for ad free version, more watches and musical tones for $0.99 cents" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"UPGRADE", nil];
        [alertView show];
    }
    else if ([cellText isEqualToString:@"SUGGESTION"]) {
        [self showMailComposeViewControllerWithRecipients:[NSArray arrayWithObject:@"Beetrisa@gmail.com"] andSubject:@"Suggestions"];
    }
    else if ([cellText isEqualToString:@"HELP"]) {
        HelpViewController *helpVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
        [self.navigationController pushViewController:helpVC animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

#pragma mark -

- (void)showMailComposeViewControllerWithRecipients:(NSArray *)recipients andSubject:(NSString *)subject {
    if([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailComposeVC = [[MFMailComposeViewController alloc] init];
        NSArray *toRecipients = [NSArray arrayWithObject:@"Beetrisa@gmail.com"];
        [mailComposeVC setToRecipients:toRecipients];
        [mailComposeVC setMailComposeDelegate:self];
        [mailComposeVC setSubject:@"Suggestions"];
        [self.navigationController presentViewController:mailComposeVC animated:YES completion:nil];
    }
    else {
        alert = [[UIAlertView alloc] initWithTitle:[Common getApplicationName] message:@"Current device is not capable of sending mail" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
}

- (void)showMessageComposeViewControllerWithText:(NSString *)textMessage {
    if([MFMessageComposeViewController canSendText]) {
        MFMessageComposeViewController *messageComposeVC = [[MFMessageComposeViewController alloc] init];
        [messageComposeVC setBody:@""];
        [self.navigationController presentViewController:messageComposeVC animated:YES completion:nil];
    }
    else {
        alert = [[UIAlertView alloc] initWithTitle:[Common getApplicationName] message:@"Current device is not capable of sending text messages" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
}

#pragma mark - MFMailCompose Delegates

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    
    switch (result) {
        case MFMailComposeResultCancelled: {
            
            [self.navigationController dismissViewControllerAnimated:YES completion:^{
                alert = [[UIAlertView alloc] initWithTitle:[Common getApplicationName] message:@"Message sending cancelled" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alert show];
            }];
            break;
        }
        
        case MFMailComposeResultSaved:
            alert = [[UIAlertView alloc] initWithTitle:[Common getApplicationName] message:@"Mail saved" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
            break;

        case MFMailComposeResultSent:
            alert = [[UIAlertView alloc] initWithTitle:[Common getApplicationName] message:@"Message sent successfullt" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
            break;
            
        case MFMailComposeResultFailed:
            alert = [[UIAlertView alloc] initWithTitle:[Common getApplicationName] message:@"Mail sending failed" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
            break;
            
        default:
            break;
    }
}

#pragma mark - MFMessageComposeViewController Delegate 

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    
    switch (result) {
        case MessageComposeResultCancelled:
            alert = [[UIAlertView alloc] initWithTitle:[Common getApplicationName] message:@"Message sending cancelled" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
            break;

        case MessageComposeResultSent:
            alert = [[UIAlertView alloc] initWithTitle:[Common getApplicationName] message:@"Message sent successfully" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
            break;

        case MessageComposeResultFailed:
            alert = [[UIAlertView alloc] initWithTitle:[Common getApplicationName] message:@"Message sending failed" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
            break;
            
        default:
            break;
    }
}

#pragma mark - UIAlertView Delegates

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0:
            
            break;
        case 1:
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/keynote/id361285480?mt=8"]];
            break;
        default:
            break;
    }
}

#pragma mark - UIActionsheet delegates

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if([actionSheet tag] == 1000) {
        switch (buttonIndex) {
            case 0:
                [Common shareOnFacebookWithMessage:nil];
            case 1:
                [Common shareOnTwitterWithMessage:nil];
                break;
            case 2:
                [Common authenticateLinkedInUserWithMessage:nil];
                break;
            case 3:
                [Common shareOnWhatsApp];
                break;
            case 4: {
                //mail
                [self showMailComposeViewControllerWithRecipients:@[] andSubject:@""];
                break;
            }
            case 5:
                [self showMessageComposeViewControllerWithText:@""];
                //SMS
                break;
            default:
                break;
        }
    }
    else if ([actionSheet tag] == 1001) {

        LikeWebViewController *likeWebViewVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LikeWebViewController"];

        switch (buttonIndex) {
            case 0:
                //facebook
                likeWebViewVC.strURL = @"https://www.facebook.com/timecandybybeetrisa";
                [self.navigationController pushViewController:likeWebViewVC animated:YES];
                break;
            case 1:
                //twitter
                likeWebViewVC.strURL = @"https://www.twitter.com/beetrisa";
                [self.navigationController pushViewController:likeWebViewVC animated:YES];
                break;
            case 2:
                //linkedin
                likeWebViewVC.strURL = @"https://www.linkedin.com/in/beetrisa";
                [self.navigationController pushViewController:likeWebViewVC animated:YES];
                break;
            case 3: {
                //youtube
                likeWebViewVC.strURL = @"https://www.youtube.com/watch?v=jisaGhhN61Y";
                [self.navigationController pushViewController:likeWebViewVC animated:YES];
                break;
            }
            case 4:
                likeWebViewVC.strURL = @"https://plus.google.com/photos/115174626591163885841/albums/6105570856894927297?sort=1";
                [self.navigationController pushViewController:likeWebViewVC animated:YES];
                break;
            case 5:
                //pinterest
                likeWebViewVC.strURL = @"http://www.pinterest.com/beetrisa/beetrisain/";
                break;
            default:
                break;
        }
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnBackPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
